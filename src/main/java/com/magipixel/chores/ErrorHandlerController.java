package com.magipixel.chores;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/error")
public class ErrorHandlerController implements ErrorController {
    @RequestMapping
    @ResponseStatus(HttpStatus.OK)
    public ModelAndView redirect() {
        System.out.println("/error mapping");
        return new ModelAndView("forward:/ui/signup");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
