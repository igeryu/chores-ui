import {clientPaths} from './client-paths';
import {paths} from './paths';
import {userRoles} from './user-roles';

export const environment = {
  production: true,

  clientId: 'userClient',
  clientSecret: 'userClientPassword',

  clientPaths,
  jwtKey: 'JWT',
  paths,
  startPage: paths.dashboard.route,
  usernameKey: 'username',
  userRoles,
};
