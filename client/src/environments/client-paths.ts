const CHILD_PATH_PREFIX = '/api/children';

export const clientPaths = {
  auth: {
    token: '/api/auth/oauth/token',
  },
  children: {
    path: CHILD_PATH_PREFIX,
    childChores: {
      prefix: `${CHILD_PATH_PREFIX}/`,
      suffix: '/chores/',
    },
  },
  availableChores: '/api/chores',
  users: '/api/credentials/users',
};
