// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {clientPaths} from './client-paths';
import {paths} from './paths';
import {userRoles} from './user-roles';

export const environment = {
  production: false,

  clientId: 'userClient',
  clientSecret: 'userClientPassword',

  clientPaths,
  jwtKey: 'JWT',
  paths,
  startPage: paths.dashboard.route,
  usernameKey: 'username',
  userRoles,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
