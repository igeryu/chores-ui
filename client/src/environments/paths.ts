const UI = 'ui';
const UI_ROUTE_PREFIX = `${UI}/`;
const UI_NAVIGATION_PREFIX = `/${UI}`;

const CHILD_ROUTE_ROOT = UI_ROUTE_PREFIX + 'child/';

const DASHBOARD = 'dashboard';

export const paths = {
  child: {
    navigation: {
      prefix: [UI_NAVIGATION_PREFIX, 'child'],
    },
    chores: {
      add: {
        route: `${CHILD_ROUTE_ROOT}:id/chores/add`,
        navigation: {
          suffix: ['chores', 'add'],
        },
      },
      assign: {
        route: `${CHILD_ROUTE_ROOT}:id/chores/assign`,
        navigation: {
          suffix: ['chores', 'assign'],
        },
      },
      assigned: {
        route: `${CHILD_ROUTE_ROOT}:id/chores`,
        navigation: {
          suffix: ['chores'],
        },
      },
    },
  },
  dashboard: {
    route: `${UI_ROUTE_PREFIX}${DASHBOARD}`,
    navigation: [UI_NAVIGATION_PREFIX, DASHBOARD],
  },
  emailConfirmation: UI_ROUTE_PREFIX + 'email-confirmation',
  signIn: {
    route: `${UI_ROUTE_PREFIX}login`,
    navigation: [UI_NAVIGATION_PREFIX, 'login'],
    path: `${UI},login`,
  },
  signUp: {
    route: `${UI_ROUTE_PREFIX}signup`,
    path: `${UI},signup`,
  },
};
