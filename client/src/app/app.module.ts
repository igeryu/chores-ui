import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from './routing/app-routing.module';
import { AppComponent } from './app.component';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignupComponent } from './components/signup/signup.component';
import { AssignedChoresComponent } from './components/child-chores/assigned-chores/assigned-chores.component';
import { AssignChoresComponent } from './components/child-chores/assign-chores/assign-chores.component';
import { AddChoreComponent } from './components/child-chores/add-chore/add-chore.component';
import { AuthService } from './services/security/auth.service';

export function tokenGetter() {
  return new AuthService(new JwtHelperService()).getAuthToken();
}

@NgModule({
  declarations: [
    AppComponent,
    BackButtonComponent,
    DashboardComponent,
    EmailConfirmationComponent,
    SignInComponent,
    SignupComponent,
    AssignedChoresComponent,
    AssignChoresComponent,
    AddChoreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    JwtModule.forRoot({
      config: {
        tokenGetter
      }
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
