import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { environment } from 'src/environments/environment';

import { AuthService } from './security/auth.service';
import { ChildService } from './child.service';
import { Child } from '../models/child';

describe('ChildService', () => {
  const ALL_CHILDREN_PATH = environment.clientPaths.children.path;
  const CHILD_PREFIX = environment.clientPaths.children.childChores.prefix;

  let service: ChildService;
  let authService: any;
  let mockHttp: HttpTestingController;

  beforeEach(() => {
    const mockTokenService = jasmine.createSpyObj('AuthService', ['getAuthToken']);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        { provide: AuthService, useValue: mockTokenService },
      ],
    });

    mockHttp = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ChildService);
    authService = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('addChild', () => {
    it('should use correct URL', () => {
      service.addChild('', '').subscribe();

      expect(mockHttp.expectOne(ALL_CHILDREN_PATH)).toBeTruthy();
    });

    it('should use POST', () => {
      service.addChild('', '').subscribe();

      const method = mockHttp.expectOne(ALL_CHILDREN_PATH).request.method;
      expect(method).toBe('POST');
    });

    it('should use \'application/json\' for \'Content-Type\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.addChild('', '').subscribe();

      // Assert
      const contentType = mockHttp.expectOne(ALL_CHILDREN_PATH).request.headers.get('Content-Type');
      expect(contentType).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.addChild('', '').subscribe();

        // Assert
        const authorization = mockHttp.expectOne(ALL_CHILDREN_PATH).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should use the correct request body', () => {
      const expectedBody: Child = {
        email: '1234abcd',
        firstName: 'abcd1234',
      };

      service.addChild('1234abcd', 'abcd1234').subscribe();

      const body = mockHttp.expectOne(ALL_CHILDREN_PATH).request.body;
      expect(body).toEqual(expectedBody);
    });

    it('should return the response', (done) => {
      service.addChild('1234abcd', 'abcd1234').subscribe(() => {
        done();
      });

      const request = mockHttp.expectOne(ALL_CHILDREN_PATH);
      expect(request).toBeTruthy();
      request.flush({});
    });
  });

  describe('getChildren', () => {
    it('should use correct URL', () => {
      service.getChildren().subscribe();

      expect(mockHttp.expectOne(ALL_CHILDREN_PATH)).toBeTruthy();
    });

    it('should use GET', () => {
      service.getChildren().subscribe();

      const method = mockHttp.expectOne(ALL_CHILDREN_PATH).request.method;
      expect(method).toBe('GET');
    });

    it('should use \'application/json\' for \'Accept\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.getChildren().subscribe();

      // Assert
      const accept = mockHttp.expectOne(ALL_CHILDREN_PATH).request.headers.get('Accept');
      expect(accept).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.getChildren().subscribe();

        // Assert
        const authorization = mockHttp.expectOne(ALL_CHILDREN_PATH).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should return the response', (done) => {
      const expectedResponse: Child[] = [
        { firstName: 'asdf_1234', email: '' },
        { firstName: '1234_asf', email: '' },
      ];
      service.getChildren().subscribe((response: Child[]) => {
        expect(response).toEqual(expectedResponse);
        done();
      });

      mockHttp.expectOne(ALL_CHILDREN_PATH).flush(expectedResponse);
    });
  });

  describe('getChild', () => {
    const childId = Math.trunc(Math.random() * 1000);

    it('should use correct URL', () => {
      service.getChild(childId).subscribe();

      expect(mockHttp.expectOne(`${CHILD_PREFIX}${childId}`)).toBeTruthy();
    });

    it('should use GET', () => {
      service.getChild(childId).subscribe();

      const method = mockHttp.expectOne(`${CHILD_PREFIX}${childId}`).request.method;
      expect(method).toBe('GET');
    });

    it('should use \'application/json\' for \'Accept\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.getChild(childId).subscribe();

      // Assert
      const accept = mockHttp.expectOne(`${CHILD_PREFIX}${childId}`).request.headers.get('Accept');
      expect(accept).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.getChild(childId).subscribe();

        // Assert
        const authorization = mockHttp.expectOne(`${CHILD_PREFIX}${childId}`).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should return the response', (done) => {
      const expectedResponse: Child = {
        firstName: 'asdf_1234',
        email: ''
      };
      service.getChild(childId).subscribe((response: Child) => {
        expect(response).toEqual(expectedResponse);
        done();
      });

      mockHttp.expectOne(`${CHILD_PREFIX}${childId}`).flush(expectedResponse);
    });
  });
});
