import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigateBackService {
  subject: Subject<any> = new Subject<null>();

  public push(callback: (() => void) | null): void {
    this.subject.next(callback);
  }

  public events(): Subject<() => void> {
    return this.subject;
  }
}
