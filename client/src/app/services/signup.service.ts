import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

const USERS_PATH = environment.clientPaths.users;

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(
    private http: HttpClient,
  ) { }

  public signup(email: string, password: string): Observable<void> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    const request = {
      username: email,
      password
    };

    return this.http.post<void>(USERS_PATH, request, headers);
  }
}
