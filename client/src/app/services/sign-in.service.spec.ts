import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { environment } from 'src/environments/environment';

import { SignInService } from './sign-in.service';
import { OauthResponse } from '../models/oauth-response';

describe('SignInService', () => {
  let service: SignInService;
  let mockHttp: HttpTestingController;
  const SIGNIN_URL = environment.clientPaths.auth.token;
  const CLIENT_ID = environment.clientId;
  const CLIENT_SECRET = environment.clientSecret;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
    });
    mockHttp = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SignInService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('signin()', () => {
    it('should use correct URL', () => {
      service.signin('', '').subscribe();

      expect(mockHttp.expectOne(SIGNIN_URL)).toBeTruthy();
    });

    it('should use POST', () => {
      service.signin('', '').subscribe();

      const method = mockHttp.expectOne(SIGNIN_URL).request.method;
      expect(method).toBe('POST');
    });

    it('should use \'application/x-www-form-urlencoded\' for \'Content-Type\'', () => {
      service.signin('', '').subscribe();

      const contentType = mockHttp.expectOne(SIGNIN_URL).request.headers.get('Content-Type');
      expect(contentType).toBe('application/x-www-form-urlencoded');
    });

    it('should use correct client ID and client secret', () => {
      service.signin('', '').subscribe();

      const contentType = mockHttp.expectOne(SIGNIN_URL).request.headers.get('Authorization');
      const expected = 'Basic ' + btoa(`${CLIENT_ID}:${CLIENT_SECRET}`);
      expect(contentType).toBe(expected);
    });

    it('should use the correct request body', () => {
      const expectedBody = `grant_type=password` + `&username=1234abcd` + `&password=abcd1234`;

      service.signin('1234abcd', 'abcd1234').subscribe();

      const body = mockHttp.expectOne(SIGNIN_URL).request.body;
      expect(body).toEqual(expectedBody);
    });

    it('should return the response', (done) => {
      const expectedToken = 'expected_token';
      service.signin('1234abcd', 'abcd1234').subscribe((response: OauthResponse) => {
        expect(response.access_token).toEqual(expectedToken);
        done();
      });

      mockHttp.expectOne(SIGNIN_URL).flush({
        access_token: expectedToken
      });
    });

  });
});
