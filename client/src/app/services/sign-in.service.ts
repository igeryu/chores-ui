import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { OauthResponse } from '../models/oauth-response';

const CLIENT_ID = environment.clientId;
const CLIENT_SECRET = environment.clientSecret;

const TOKEN_PATH = environment.clientPaths.auth.token;

@Injectable({
  providedIn: 'root'
})
export class SignInService {

  constructor(
    private http: HttpClient,
    ) { }

  public signin(email: string, password: string): Observable<OauthResponse> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Basic ' + btoa(`${CLIENT_ID}:${CLIENT_SECRET}`),
      })
    };

    const request = `grant_type=password&username=${email}&password=${password}`;

    return this.http.post<OauthResponse>(TOKEN_PATH, request, headers);
  }
}
