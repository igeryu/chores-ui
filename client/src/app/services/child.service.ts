import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';

import { environment } from 'src/environments/environment';

import { AuthService } from './security/auth.service';

import { Child } from '../models/child';

const ALL_CHILDREN_PATH = environment.clientPaths.children.path;
const CHILD_PREFIX = environment.clientPaths.children.childChores.prefix;

@Injectable({
  providedIn: 'root'
})
export class ChildService {

  constructor(
    private http: HttpClient,
    private tokenService: AuthService,
    ) { }

  public addChild(email: string, firstName: string): Observable<void> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    const requestBody = { email, firstName } as Child;

    return this.http.post<void>(ALL_CHILDREN_PATH, requestBody, headers);
  }

  public getChildren(): Observable<Child[]> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    return this.http.get<Child[]>(ALL_CHILDREN_PATH, headers);
  }

  public getChild(childId: number): Observable<Child> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    return this.http.get<Child>(`${CHILD_PREFIX}${childId}`, headers);
  }
}
