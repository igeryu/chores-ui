import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { AuthService } from './security/auth.service';
import { Chore } from '../models/chore';

const CHORE_URL = environment.clientPaths.availableChores;
const CHILD_CHORES_PREFIX = environment.clientPaths.children.childChores.prefix;
const CHILD_CHORES_SUFFIX = environment.clientPaths.children.childChores.suffix;

@Injectable({
  providedIn: 'root'
})
export class ChoreService {

  constructor(
    private http: HttpClient,
    private tokenService: AuthService,
  ) { }

  public addAvailableChore(name: string): Observable<void> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    const chore = ({ name } as Chore);
    return this.http.post<void>(CHORE_URL, chore, headers);
  }

  public getAvailableChores(): Observable<Chore[]> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    return this.http.get<Chore[]>(CHORE_URL, headers);
  }

  public assignChore(
    childId: number,
    chore: Chore): Observable<void> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    return this.http.put<void>(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`, chore, headers);
  }

  public getAssignedChores(childId: number): Observable<Chore[]> {
    const token = this.tokenService.getAuthToken();
    const headers = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      })
    };
    return this.http.get<Chore[]>(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`, headers);
  }
}
