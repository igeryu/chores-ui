import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

const PARENT = environment.userRoles.parent;
const CHILD_CHORES_PATH = environment.paths.child.chores.assigned.route.split(',')[1];
const DASHBOARD_PATH = environment.paths.dashboard.route.split('/')[1];

@Injectable({
  providedIn: 'root'
})
export class RouteAccessService {

  isUserAllowedToAccess(userRole: string, route: string): boolean {

    const path = route.split('/')[1];
    switch (path) {
      case DASHBOARD_PATH:
        switch (userRole) {
          case PARENT:
            return true;
        }
        break;

      case CHILD_CHORES_PATH:
        switch (userRole) {
          case PARENT:
            return true;
        }
        break;
    }
    return false;
  }
}
