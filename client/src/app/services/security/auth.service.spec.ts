import { JwtHelperService } from '@auth0/angular-jwt';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { environment } from '../../../environments/environment';

describe('AuthService', () => {
  let service: AuthService;
  let jwtHelper: jasmine.SpyObj<JwtHelperService>;

  beforeEach(() => {
    const mockJwtHelper = jasmine.createSpyObj(['isTokenExpired']);

    TestBed.configureTestingModule({
      providers: [
        {provide: JwtHelperService, useValue: mockJwtHelper},
      ],
    });
    service = TestBed.inject(AuthService);
    jwtHelper = TestBed.inject(JwtHelperService) as jasmine.SpyObj<JwtHelperService>;
  });

  afterEach(() => sessionStorage.clear());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('isAuthenticated', () => {

    it('should call method and return true if JWT token is not expired', () => {
      jwtHelper.isTokenExpired.and.returnValue(false);
      spyOn(sessionStorage, 'getItem').and.returnValue('jwt_token');

      const result = service.isAuthenticated();

      expect(result).toBe(true);
      expect(jwtHelper.isTokenExpired).toHaveBeenCalled();
    });

    it('should return false if JWT token is expired', () => {
      jwtHelper.isTokenExpired.and.returnValue(true);
      spyOn(sessionStorage, 'getItem').and.returnValue('jwt_token');

      const result = service.isAuthenticated();

      expect(result).toBe(false);
    });

    it('should return false if JWT token is null', () => {
      jwtHelper.isTokenExpired.and.returnValue(false);
      spyOn(sessionStorage, 'getItem').and.returnValue(null);

      const result = service.isAuthenticated();

      expect(result).toBe(false);
    });

  });

  describe('currentUser', () => {
    it('should save the currentUser to sessionStorage', (done) => {
      // Warning: Even though this test asserts that AuthService uses session storage,
      //          other classes should call the getCurrentUser method, and not access the
      //          current user via sessionStorage
      const expectedUser = {id: 456, gitlabId: 123, name: 'dummy user', role: 'dummy role'};
      service.setCurrentUser(expectedUser);

      const secondService = new AuthService(jwtHelper);
      secondService.getCurrentUser().then((actualUser) => {
        expect(actualUser).toEqual(expectedUser);
        done();
      });
    });
  });

  describe('getAuthToken', () => {
    it('should return the correct key from sessionStorage', () => {
      spyOn(sessionStorage, 'getItem').and.returnValue('anticipation');

      const response = service.getAuthToken();

      expect(response).toEqual('anticipation');
    });
  });

  describe('setAuthToken', () => {
    it('should set the correct key in sessionStorage', () => {
      spyOn(sessionStorage, 'setItem');

      service.setAuthToken('dynamics');

      expect(sessionStorage.setItem).toHaveBeenCalledWith(environment.jwtKey, 'dynamics');
    });
  });

  describe('setUsername', () => {
    it('should set the correct key in sessionStorage', () => {
      spyOn(sessionStorage, 'setItem');

      service.setUsername('wading');

      expect(sessionStorage.setItem).toHaveBeenCalledWith(environment.usernameKey, 'wading');
    });
  });

  describe('logout', () => {
    it('should call clear on sessionStorage', () => {
      spyOn(sessionStorage, 'clear');

      service.logout();

      expect(sessionStorage.clear).toHaveBeenCalledWith();
    });

    it('should clear out current user', (done) => {
      service.setCurrentUser({role: ''});

      service.logout();

      service.getCurrentUser().then(
        () => {
          throw new Error('Expected this promise to be rejected');
        }, () => {
          expect(null).toBeNull();
          done();
        });
    });
  });
});
