import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from '../../../environments/environment';
import { User } from 'src/app/models/user';

const CURRENT_USER_KEY = 'currentUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private jwtHelper: JwtHelperService,
  ) { }

  isAuthenticated(): boolean {
    const token = this.getAuthToken();
    return token !== null && !this.jwtHelper.isTokenExpired(token);
  }

  getCurrentUser(): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      const currentUserEncoded = sessionStorage.getItem(CURRENT_USER_KEY);
      if (currentUserEncoded) {
        resolve(JSON.parse(atob(currentUserEncoded)));
      } else {
        reject();
      }
    });
  }

  setCurrentUser(currentUser: User): void {
    const currentUserEncoded = btoa(JSON.stringify(currentUser));
    sessionStorage.setItem(CURRENT_USER_KEY, currentUserEncoded);
  }

  getAuthToken(): string | null {
    const key = environment.jwtKey;
    return sessionStorage.getItem(key);
  }

  setAuthToken(token: string): void {
    const key = environment.jwtKey;
    sessionStorage.setItem(key, token);
  }

  setUsername(username: string): void {
    const key = environment.usernameKey;
    sessionStorage.setItem(key, username);
  }

  logout(): void {
    sessionStorage.clear();
  }
}
