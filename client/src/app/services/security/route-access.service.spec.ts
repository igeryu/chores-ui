import { TestBed } from '@angular/core/testing';

import { environment } from 'src/environments/environment';

import { RouteAccessService } from './route-access.service';

describe('RouteAccessService', () => {
  let service: RouteAccessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouteAccessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  [
    {
      view: 'Dashboard',
      route: 'ui,dashboard',
      roles: [
        {role: 'Parent', canAccess: true },
      ],
    },
    {
      view: 'Child Chores',
      route: 'ui,child,ChildName,chores',
      roles: [
        {role: 'Parent', canAccess: true },
      ],
    },
  ].forEach(({view, route, roles}) => {
    roles.forEach(({role, canAccess}) => {
      it(`and a ${role}, and accessing the "${view}" view, should return ${canAccess}`, () => {
        const userRole = role.toUpperCase().replace(' ', '_');

        expect(service.isUserAllowedToAccess(userRole, route)).toBe(canAccess);
      });
    });
  });

});
