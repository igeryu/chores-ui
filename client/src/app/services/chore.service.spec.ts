import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { environment } from 'src/environments/environment';

import { AuthService } from './security/auth.service';
import { ChoreService } from './chore.service';
import { Chore } from '../models/chore';

const CHORE_URL = environment.clientPaths.availableChores;
const CHILD_CHORES_PREFIX = environment.clientPaths.children.childChores.prefix;
const CHILD_CHORES_SUFFIX = environment.clientPaths.children.childChores.suffix;

describe('ChoreService', () => {
  let service: ChoreService;
  let authService: jasmine.SpyObj<AuthService>;
  let mockHttp: HttpTestingController;

  beforeEach(() => {
    const mockTokenService = jasmine.createSpyObj('AuthService', ['getAuthToken']);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        { provide: AuthService, useValue: mockTokenService },
      ],
    });

    mockHttp = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ChoreService);
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('addAvailableChore', () => {
    it('should use correct URL', () => {
      service.addAvailableChore('').subscribe();

      expect(mockHttp.expectOne(CHORE_URL)).toBeTruthy();
    });

    it('should use POST', () => {
      service.addAvailableChore('').subscribe();

      const method = mockHttp.expectOne(CHORE_URL).request.method;
      expect(method).toBe('POST');
    });

    it('should use \'application/json\' for \'Content-Type\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.addAvailableChore('').subscribe();

      // Assert
      const contentType = mockHttp.expectOne(CHORE_URL).request.headers.get('Content-Type');
      expect(contentType).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.addAvailableChore('').subscribe();

        // Assert
        const authorization = mockHttp.expectOne(CHORE_URL).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should use the correct request body', () => {
      const expectedBody: Chore = {
        name: '1234abcd',
      };

      service.addAvailableChore('1234abcd').subscribe();

      const body = mockHttp.expectOne(CHORE_URL).request.body;
      expect(body).toEqual(expectedBody);
    });

    it('should return the response', (done) => {
      service.addAvailableChore('1234abcd').subscribe(() => {
        done();
      });

      const request = mockHttp.expectOne(CHORE_URL);
      expect(request).toBeTruthy();
      request.flush({});
    });
  });

  describe('getAvailableChores', () => {
    it('should use correct URL', () => {
      service.getAvailableChores().subscribe();

      expect(mockHttp.expectOne(CHORE_URL)).toBeTruthy();
    });

    it('should use GET', () => {
      service.getAvailableChores().subscribe();

      const method = mockHttp.expectOne(CHORE_URL).request.method;
      expect(method).toBe('GET');
    });

    it('should use \'application/json\' for \'Accept\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.getAvailableChores().subscribe();

      // Assert
      const accept = mockHttp.expectOne(CHORE_URL).request.headers.get('Accept');
      expect(accept).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.getAvailableChores().subscribe();

        // Assert
        const authorization = mockHttp.expectOne(CHORE_URL).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should return the response', (done) => {
      const expectedResponse: Chore[] = [
        { name: 'asdf_1234' },
        { name: '1234_asf' },
      ];
      service.getAvailableChores().subscribe((response: Chore[]) => {
        expect(response).toEqual(expectedResponse);
        done();
      });

      mockHttp.expectOne(CHORE_URL).flush(expectedResponse);
    });
  });

  describe('assignChore', () => {
    const childId = Math.trunc(Math.random() * 1000);

    it('should use correct URL', () => {
      service.assignChore(childId, { name: '' }).subscribe();

      expect(mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`)).toBeTruthy();
    });

    it('should use PUT', () => {
      service.assignChore(childId, { name: '' }).subscribe();

      const method = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.method;
      expect(method).toBe('PUT');
    });

    it('should use \'application/json\' for \'Content-Type\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.assignChore(childId, { name: '' }).subscribe();

      // Assert
      const contentType = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.headers.get('Content-Type');
      expect(contentType).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.assignChore(childId, { name: '' }).subscribe();

        // Assert
        const authorization =
        mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should use the correct request body', () => {
      const expectedBody: Chore = {
        name: '1234abcd',
      };

      service.assignChore(childId, expectedBody).subscribe();

      const body = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.body;
      expect(body).toEqual(expectedBody);
    });

    it('should return the response', (done) => {
      service.assignChore(childId, { name: '' }).subscribe(() => {
        done();
      });

      const request = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`);
      expect(request).toBeTruthy();
      request.flush({});
    });
  });

  describe('getAssignedChores', () => {
    const childId = Math.trunc(Math.random() * 1000);

    it('should use correct URL', () => {
      service.getAssignedChores(childId).subscribe();

      expect(mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`)).toBeTruthy();
    });

    it('should use GET', () => {
      service.getAssignedChores(childId).subscribe();

      const method = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.method;
      expect(method).toBe('GET');
    });

    it('should use \'application/json\' for \'Accept\'', () => {
      // Arrange
      authService.getAuthToken.and.returnValue('');

      // Act
      service.getAssignedChores(childId).subscribe();

      // Assert
      const accept = mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.headers.get('Accept');
      expect(accept).toBe('application/json');
    });

    [
      'asdf_1234',
      '1234_asdf',
    ].forEach((expectedToken) => {
      it('should use OAuth 2 bearer token for \'Authorization\'', () => {
        // Arrange
        authService.getAuthToken.and.returnValue(expectedToken);

        // Act
        service.getAssignedChores(childId).subscribe();

        // Assert
        const authorization =
        mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).request.headers.get('Authorization');
        expect(authorization).toBe(`Bearer ${expectedToken}`);
      });
    });

    it('should return the response', (done) => {
      const expectedResponse: Chore[] = [
        { name: 'asdf_1234' },
        { name: '1234_asf' },
      ];
      service.getAssignedChores(childId).subscribe((response: Chore[]) => {
        expect(response).toEqual(expectedResponse);
        done();
      });

      mockHttp.expectOne(`${CHILD_CHORES_PREFIX}${childId}${CHILD_CHORES_SUFFIX}`).flush(expectedResponse);
    });
  });

});
