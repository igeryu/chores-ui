import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SignupService } from './signup.service';

import { environment } from 'src/environments/environment';

describe('SignupService', () => {
  let service: SignupService;
  let mockHttp: HttpTestingController;
  const SIGNUP_URL = environment.clientPaths.users;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
    });
    mockHttp = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SignupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('signup()', () => {
    it('should use correct URL', () => {
      service.signup('', '').subscribe();

      mockHttp.expectOne(SIGNUP_URL);
    });

    it('should use POST', () => {
      service.signup('', '').subscribe();

      const method = mockHttp.expectOne(SIGNUP_URL).request.method;
      expect(method).toBe('POST');
    });

    it('should use \'application/json\' for \'Content-Type\'', () => {
      service.signup('', '').subscribe();

      const contentType = mockHttp.expectOne(SIGNUP_URL).request.headers.get('Content-Type');
      expect(contentType).toBe('application/json');
    });

    it('should use the correct request body', () => {
      const expectedBody = {
        username: '1234abcd',
        password: 'abcd1234'
      };

      service.signup('1234abcd', 'abcd1234').subscribe();

      const body = mockHttp.expectOne(SIGNUP_URL).request.body;
      expect(body).toEqual(expectedBody);
    });

    it('should return the response', (done) => {
      service.signup('1234abcd', 'abcd1234').subscribe(() => {
        done();
      });

      mockHttp.expectOne(SIGNUP_URL).flush({});
    });

  });
});
