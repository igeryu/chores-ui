import { TestBed } from '@angular/core/testing';

import { NavigateBackService } from './navigate-back.service';

describe('NavigateBackService', () => {
  let service: NavigateBackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavigateBackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should emit the provided function', (done) => {
    // Assert (async)
    const pushedCallback = () => {
      done();
    };
    service.events().subscribe((callback) => {
      callback();
    });

    // Act
    service.push(pushedCallback);
  });

  it('should emit a null function', (done) => {
    // Assert (async)
    service.events().subscribe((callback) => {
      done();
    });

    // Act
    service.push(null);
  });
});
