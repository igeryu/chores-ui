import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { Chore } from 'src/app/models/chore';
import { ChildService } from 'src/app/services/child.service';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { AssignedChoresComponent } from './assigned-chores.component';

describe('AssignedChoresComponent', () => {
  let component: AssignedChoresComponent;
  let fixture: ComponentFixture<AssignedChoresComponent>;

  beforeEach(waitForAsync(() => {
    const mockRouter = jasmine.createSpyObj('Router', ['navigate']);

    const mockActivatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
    mockActivatedRoute.params = of({});

    const mockChildService = jasmine.createSpyObj('ChildService', ['getChild']);
    mockChildService.getChild.and.returnValue(of(void 0));

    const mockChoreService = jasmine.createSpyObj('ChoreService', ['getAssignedChores']);
    mockChoreService.getAssignedChores.and.returnValue(of(void 0));

    const mockNavigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

    TestBed.configureTestingModule({
      declarations: [AssignedChoresComponent],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: Router, useValue: mockRouter },
        { provide: ChildService, useValue: mockChildService },
        { provide: ChoreService, useValue: mockChoreService },
        { provide: NavigateBackService, useValue: mockNavigateBackService },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedChoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display child\'s name in header', () => {
    const expectedFirstName = 'expected_child_name';
    component.selectedChild = { firstName: expectedFirstName, email: '' };
    fixture.detectChanges();

    const header = fixture.debugElement.query(By.css('h1')).nativeElement;

    expect(header).toBeTruthy();
    expect(header.textContent).toEqual(expectedFirstName + '\'s Assigned Chores');
  });

  describe('in the Assigned Chore list,', () => {

    it('given no Chores, should not be visible', () => {
      component.choresAssigned = [];
      fixture.detectChanges();

      const assignedChoresList = fixture.debugElement.query(By.css('[data-cy=assigned-chore-list]'));
      expect(assignedChoresList).toBeTruthy();

      const assignedChore = fixture.debugElement.query(By.css('[data-cy=assigned-chore]'));
      expect(assignedChore).toBeFalsy();
    });

    describe('given one Assigned Chore,', () => {
      const expectedName = 'expected_name';

      beforeEach(() => {
        component.choresAssigned = [{ name: expectedName }] as Chore[];
        fixture.detectChanges();
      });

      it('then one Assigned Chore button should be visible', () => {
        const assignedChoresList = fixture.debugElement.query(By.css('[data-cy=assigned-chore-list]'));
        expect(assignedChoresList).toBeTruthy();

        const assignedChore = fixture.debugElement.query(By.css('[data-cy=assigned-chore]'));
        expect(assignedChore).toBeTruthy();

        expect(assignedChore.nativeElement.textContent).toContain(expectedName);
      });
    });

    it('given two Assigned Chores, two Chore buttons should be visible', () => {
      const expectedNameA = 'expected_name_asdf';
      const expectedNameB = 'expected_name_1234';
      component.choresAssigned = [
        { name: expectedNameA } as Chore,
        { name: expectedNameB } as Chore,
      ];
      fixture.detectChanges();

      const assignedChoresList = fixture.debugElement.query(By.css('[data-cy=assigned-chore-list]'));
      expect(assignedChoresList).toBeTruthy();

      const assignedChores = fixture.debugElement.queryAll(By.css('[data-cy=assigned-chore]'));
      expect(assignedChores).toBeTruthy();

      expect(assignedChores.length).toEqual(2);
      expect(assignedChores[0].nativeElement.textContent).toContain(expectedNameA);
      expect(assignedChores[1].nativeElement.textContent).toContain(expectedNameB);
    });

    describe('Assign Chore button,', () => {

      beforeEach(() => {
        fixture.detectChanges();
      });

      it('should be visible', () => {
        const assignChoreButton = fixture.debugElement.query(By.css('[data-cy=assign-chore]'));

        expect(assignChoreButton).toBeTruthy();
      });

      it('should navigate to Assign Chore screen', () => {
        const navigateToAssignChoreScreen = spyOn(component, 'navigateToAssignChoreScreen');
        const assignChoreButton = fixture.debugElement.query(By.css('[data-cy=assign-chore]')).nativeElement;

        assignChoreButton.click();

        expect(navigateToAssignChoreScreen).toHaveBeenCalled();
      });

    });

  });
});
