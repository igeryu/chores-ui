import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Child } from 'src/app/models/child';
import { ChildService } from 'src/app/services/child.service';
import { Chore } from 'src/app/models/chore';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { environment } from 'src/environments/environment';

const DASHBOARD_ROUTE = environment.paths.dashboard.navigation;

@Component({
  selector: 'app-assigned-chores',
  templateUrl: './assigned-chores.component.html',
  styleUrls: ['./assigned-chores.component.css']
})
export class AssignedChoresComponent implements OnInit {
  selectedChild: Child | undefined;
  choresAssigned: Chore[] | undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private childService: ChildService,
    private choreService: ChoreService,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const childId = params.id;
      this.childService.getChild(childId).subscribe((child) => {
        this.selectedChild = child;
        this.choreService.getAssignedChores(childId).subscribe((assignedChores) => {
          this.choresAssigned = assignedChores;
        });
      });
    });

    this.navigateBackService.push(() => {
      this.router.navigate(DASHBOARD_ROUTE);
    });
  }

  public navigateToAssignChoreScreen(): void {
    if (!this.selectedChild) {
      return;
    }

    let path = environment.paths.child.navigation.prefix.slice();
    path.push(`${this.selectedChild.id}`);
    path = path.concat(environment.paths.child.chores.assign.navigation.suffix.slice());
    this.router.navigate(path);
  }

}
