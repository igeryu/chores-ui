import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { AssignedChoresComponent } from './assigned-chores.component';
import { Child } from 'src/app/models/child';
import { ChildService } from 'src/app/services/child.service';
import { Chore } from 'src/app/models/chore';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';
import { environment } from 'src/environments/environment';

describe('AssignedChoresComponent (unit)', () => {
    let component: AssignedChoresComponent;

    let routeSnapshot: jasmine.SpyObj<ActivatedRoute>;
    let router: jasmine.SpyObj<Router>;
    let childService: jasmine.SpyObj<ChildService>;
    let choreService: jasmine.SpyObj<ChoreService>;
    let navigateBackService: jasmine.SpyObj<NavigateBackService>;

    beforeEach(() => {
        routeSnapshot = jasmine.createSpyObj('ActivatedRoute', ['']);
        router = jasmine.createSpyObj('Router', ['navigate']);
        childService = jasmine.createSpyObj('ChildService', ['getChild']);
        choreService = jasmine.createSpyObj('ChoreService', ['getAssignedChores']);
        navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

        component = new AssignedChoresComponent(routeSnapshot, router, childService, choreService, navigateBackService);
    });

    describe('ngOnInit', () => {
        const expectedChildName = 'expected_name';
        const expectedChildId = 5678;
        const expectedChores: Chore[] = [
            { name: 'Chore_asdf_1234' },
            { name: 'Chore_1234_asf' },
        ];
        const expectedChild: Child = {
            id: expectedChildId,
            firstName: expectedChildName,
            email: 'expected_email'
        };

        beforeEach(() => {
            // Arrange
            routeSnapshot.params = of({ name: expectedChildId });
            component.choresAssigned = [];
            choreService.getAssignedChores.and.returnValue(of(expectedChores));
            childService.getChild.and.returnValue(of(expectedChild));

        });

        it('should get Child\'s ID from path parameters', () => {
            // Act
            component.ngOnInit();

            // Assert
            expect(component.selectedChild).toEqual(expectedChild);
        });

        it('should update list of Assigned Chores', () => {
            // Act
            component.ngOnInit();

            // Assert
            if (!component.choresAssigned) {
                fail('Expected choresAssigned field to be set');
                return;
            }
            expect(component.choresAssigned.length).toEqual(expectedChores.length);
            for (let x = 0; x < expectedChores.length; x++) {
                expect(component.choresAssigned[x]).toEqual(expectedChores[x]);
            }
        });

        it('should push a "display Dashboard" call back to NavigateBackService', (done) => {
            // Arrange
            navigateBackService.push.and.callFake((pushedValue) => {
                // Validating pushed function
                // Act
                expect(router.navigate).toHaveBeenCalledTimes(0);
                expect(pushedValue).toBeTruthy('Expected callback function to be pushed');
                if (!pushedValue) {
                    fail('Expected callback function to be pushed');
                    return;
                }
                pushedValue();

                // Assert
                expect(router.navigate).toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate).toHaveBeenCalledWith(environment.paths.dashboard.navigation);
                done();
            });

            // Act
            component.ngOnInit();

            // Assert
            expect(navigateBackService.push).toHaveBeenCalled();
            expect(navigateBackService.push).toHaveBeenCalledTimes(1);
        });
    });

    describe('navigateToAssignChoreScreen()', () => {
        it('should navigate to Assign Chore screen', () => {
            // Arrange
            const expectedChildId = 12345;
            component.selectedChild = {
                id: expectedChildId,
                firstName: '',
                email: '',
            };

            // Act
            component.navigateToAssignChoreScreen();

            // Assert
            expect(router.navigate).toHaveBeenCalled();
            const expectedPath = ['/ui', 'child', `${expectedChildId}`, 'chores', 'assign'];
            expect(router.navigate).toHaveBeenCalledWith(expectedPath);
        });

    });
});
