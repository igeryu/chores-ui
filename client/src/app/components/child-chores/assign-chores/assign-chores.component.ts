import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Child } from 'src/app/models/child';
import { ChildService } from 'src/app/services/child.service';
import { Chore } from 'src/app/models/chore';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-assign-chores',
  templateUrl: './assign-chores.component.html',
  styleUrls: ['./assign-chores.component.css']
})
export class AssignChoresComponent implements OnInit {
  selectedChild: Child = { firstName: '', email: '' };
  availableChores: Chore[] = [];
  selectedChore: Chore | undefined;
  assignChoreConfirmation = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private childService: ChildService,
    private choreService: ChoreService,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const childId = params.id;
      this.childService.getChild(childId).subscribe((child) => {
        this.selectedChild = child;
      });
    });

    this.choreService.getAvailableChores().subscribe((availableChores) => {
      this.availableChores = availableChores;
    });

    this.navigateBackService.push(() => {
      this.navigateToChildsAssignedChores();
    });
  }

  public cancelAssignmentOfChore(): void {
    this.navigateToChildsAssignedChores();
  }

  public confirmAssignmentOfChore(): void {
    if (!this.selectedChild.id || !this.selectedChore) {
      return;
    }

    this.choreService.assignChore(this.selectedChild.id, this.selectedChore).subscribe(() => {
      this.navigateToChildsAssignedChores();
    }, () => { });
  }

  public navigateToAddChoreScreen(): void {
    let path = environment.paths.child.navigation.prefix.slice();
    path.push(`${this.selectedChild.id}`);
    path = path.concat(environment.paths.child.chores.add.navigation.suffix.slice());
    this.router.navigate(path);
  }

  public onSelectChoreForAssignment(chore: Chore): void {
    this.assignChoreConfirmation = !this.assignChoreConfirmation;
    this.selectedChore = chore;
  }

  private navigateToChildsAssignedChores(): void {
    let path = environment.paths.child.navigation.prefix.slice();
    path.push(`${this.selectedChild.id}`);
    path = path.concat(environment.paths.child.chores.assigned.navigation.suffix.slice());
    this.router.navigate(path);
  }

}
