import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { AssignChoresComponent } from './assign-chores.component';
import { ChildService } from 'src/app/services/child.service';
import { Chore } from 'src/app/models/chore';
import { ChoreService } from 'src/app/services/chore.service';

describe('AssignChoresComponent', () => {
  let component: AssignChoresComponent;
  let fixture: ComponentFixture<AssignChoresComponent>;

  beforeEach(waitForAsync(() => {
    const mockActivatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
    mockActivatedRoute.params = of({});

    const mockRouter = jasmine.createSpyObj('Router', ['navigate']);

    const mockChildService = jasmine.createSpyObj('ChildService', [
      'getChild',
    ]);
    mockChildService.getChild.and.returnValue(of({}));

    const mockChoreService = jasmine.createSpyObj('ChoreService', ['getAvailableChores']);
    mockChoreService.getAvailableChores.and.returnValue(of(void 0));

    TestBed.configureTestingModule({
      declarations: [AssignChoresComponent],

      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: Router, useValue: mockRouter },
        { provide: ChildService, useValue: mockChildService },
        { provide: ChoreService, useValue: mockChoreService },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignChoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.assignChoreConfirmation = false;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display child\'s name in header', () => {
    const expectedFirstName = 'expected_child_name';
    component.selectedChild = { firstName: expectedFirstName, email: '' };
    fixture.detectChanges();

    const header = fixture.debugElement.query(By.css('h1')).nativeElement;

    expect(header).toBeTruthy();
    expect(header.textContent).toEqual('Assign Chore to ' + expectedFirstName);
  });

  describe('Add Chore button,', () => {

    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should be visible', () => {
      const addChoreButton = fixture.debugElement.query(By.css('[data-cy=add-chore]'));

      expect(addChoreButton).toBeTruthy();
    });

    it('should call navigateToAddChoreScreen method', () => {
      const navigateToAddChoreScreen = spyOn(component, 'navigateToAddChoreScreen');
      const addChoreButton = fixture.debugElement.query(By.css('[data-cy=add-chore]')).nativeElement;

      addChoreButton.click();

      expect(navigateToAddChoreScreen).toHaveBeenCalled();
    });

  });

  describe('in the Available Chore list,', () => {

    it('given no chores, should not be visible', () => {
      component.availableChores = [];
      fixture.detectChanges();

      const availableChore = fixture.debugElement.query(By.css('[data-cy=available-chore]'));

      expect(availableChore).toBeFalsy();
    });

    describe('given one Available Chore,', () => {
      const expectedName = 'expected_name';

      beforeEach(() => {
        component.availableChores = [{ name: expectedName }] as Chore[];
        fixture.detectChanges();
      });

      it('then one Available Chore button should be visible', () => {
        const availableChore = fixture.debugElement.query(By.css('[data-cy=available-chore]'));

        expect(availableChore).toBeTruthy();
        expect(availableChore.nativeElement.textContent).toContain(expectedName);
      });

      it('clicking a Available Chore button should call the onSelectChoreForAssignment() method', () => {
        // Arrange
        const onSelectChoreForAssignment = spyOn(component, 'onSelectChoreForAssignment');
        const availableChoreButton = fixture.debugElement.query(By.css('[data-cy=available-chore]')).nativeElement;

        // Act
        availableChoreButton.click();

        // Assert
        expect(onSelectChoreForAssignment).toHaveBeenCalled();
        expect(onSelectChoreForAssignment).toHaveBeenCalledWith({ name: expectedName });
      });
    });

    it('given two chores, two Available Chores buttons should be visible', () => {
      const expectedNameA = 'expected_name_asdf';
      const expectedNameB = 'expected_name_1234';
      component.availableChores = [
        { name: expectedNameA } as Chore,
        { name: expectedNameB } as Chore,
      ];
      fixture.detectChanges();

      const availableChores = fixture.debugElement.queryAll(By.css('[data-cy=available-chore]'));

      expect(availableChores).toBeTruthy();
      expect(availableChores.length).toEqual(2);
      expect(availableChores[0].nativeElement.textContent).toContain(expectedNameA);
      expect(availableChores[1].nativeElement.textContent).toContain(expectedNameB);
    });

  });

});
