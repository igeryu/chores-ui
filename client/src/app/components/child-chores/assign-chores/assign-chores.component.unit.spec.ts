import {
    ActivatedRoute,
    Router,
} from '@angular/router';
import {
    of,
    throwError,
} from 'rxjs';

import { AssignChoresComponent } from './assign-chores.component';
import { Child } from 'src/app/models/child';
import { Chore } from 'src/app/models/chore';
import { ChildService } from 'src/app/services/child.service';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

describe('AssignChoresComponent (unit)', () => {
    let component: AssignChoresComponent;
    let mockRouteSnapshot: jasmine.SpyObj<ActivatedRoute>;
    let choreService: jasmine.SpyObj<ChoreService>;
    let childService: jasmine.SpyObj<ChildService>;
    let navigateBackService: jasmine.SpyObj<NavigateBackService>;
    let router: jasmine.SpyObj<Router>;

    beforeEach(() => {
        mockRouteSnapshot = jasmine.createSpyObj('ActivatedRoute', ['']);

        choreService = jasmine.createSpyObj('ChoreService',
            [
                'getAvailableChores',
                'assignChore',
            ]);

        childService = jasmine.createSpyObj('ChildService', ['getChild']);
        navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);
        router = jasmine.createSpyObj('Router', ['navigate']);

        component = new AssignChoresComponent(mockRouteSnapshot, router, childService, choreService, navigateBackService);
    });

    describe('after initializing,', () => {

        it('should have assignChoreConfirmation set to false', () => {
            expect(component.assignChoreConfirmation).toEqual(false);
        });

        it('should have an empty Available Chores list', () => {
            expect(component.availableChores).toEqual([]);
        });
    });

    describe('ngOnInit', () => {
        const expectedChildName = 'expected_name';
        const expectedChildId = 5678;
        const expectedChild: Child = {
            id: expectedChildId,
            firstName: expectedChildName,
            email: 'expected_email'
        };
        const expectedChores: Chore[] = [
            { name: 'asdf_1234' },
            { name: '1234_asf' },
        ];

        beforeEach(() => {
            // Arrange
            mockRouteSnapshot.params = of({ name: expectedChildId });
            component.availableChores = [];
            childService.getChild.and.returnValue(of(expectedChild));
            choreService.getAvailableChores.and.returnValue(of(expectedChores));
        });

        it('should get Child\'s ID from path parameters', () => {
            // Act
            component.ngOnInit();

            // Assert
            expect(component.selectedChild).toEqual(expectedChild);
        });

        it('should update list of Available Chores', () => {
            // Act
            component.ngOnInit();

            // Assert
            expect(component.availableChores.length).toEqual(expectedChores.length);
            for (let x = 0; x < expectedChores.length; x++) {
                expect(component.availableChores[x]).toEqual(expectedChores[x]);
            }
        });

        it('should push a "display Assigned Chores screen" call back to NavigateBackService', (done) => {
            // Arrange
            navigateBackService.push.and.callFake((pushedValue) => {
                // Validating pushed function
                // Act
                expect(router.navigate).toHaveBeenCalledTimes(0);
                if (!pushedValue) {
                    fail('Expected callback function to be pushed');
                    return;
                }
                pushedValue();

                // Assert
                expect(router.navigate).toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate).toHaveBeenCalledWith(['/ui', 'child', `${expectedChildId}`, 'chores']);
                done();
            });

            // Act
            component.ngOnInit();

            // Assert
            expect(navigateBackService.push).toHaveBeenCalled();
            expect(navigateBackService.push).toHaveBeenCalledTimes(1);
        });
    });

    describe('cancelAssignmentOfChore()', () => {
        const expectedChildId = Math.trunc(Math.random() * 1000);

        beforeEach(() => {
            const child: Child = {
                id: expectedChildId,
                firstName: '',
                email: ''
            };
            // Arrange
            component.selectedChild = child;
            component.assignChoreConfirmation = true;
        });

        it('should navigate to Child\'s Assigned Chores screen', () => {
            // Act
            component.cancelAssignmentOfChore();

            // Assert
            expect(router.navigate).toHaveBeenCalled();
            const expectedPath = ['/ui', 'child', `${expectedChildId}`, 'chores'];
            expect(router.navigate).toHaveBeenCalledWith(expectedPath);
        });
    });

    describe('confirmAssignmentOfChore()', () => {
        const expectedChildId = Math.trunc(Math.random() * 1000);
        const expectedChore: Chore = { id: 5678, name: 'some Chore' };

        beforeEach(() => {
            const child: Child = {
                id: expectedChildId,
                firstName: '',
                email: ''
            };
            component.selectedChild = child;
            component.selectedChore = expectedChore;
            choreService.assignChore.and.returnValue(of(void 0));
        });

        it('should subscribe to result from assignChore on ChoreService', () => {
            const result = jasmine.createSpyObj('Observable<void>', ['subscribe']);
            // Arrange
            choreService.assignChore.and.returnValue(result);

            // Act
            component.confirmAssignmentOfChore();

            // Assert
            expect(choreService.assignChore).toHaveBeenCalled();
            expect(choreService.assignChore).toHaveBeenCalledTimes(1);
            expect(choreService.assignChore).toHaveBeenCalledWith(expectedChildId, expectedChore);
            expect(result.subscribe).toHaveBeenCalled();
        });

        describe('is successful,', () => {
            const choreId = Math.trunc(Math.random() * 1000);

            beforeEach(() => {
                // Arrange
                component.assignChoreConfirmation = true;
                if (!component.selectedChore) {
                    fail('Selected chore should be set');
                    return;
                }
                component.selectedChore.id = choreId;

                // Act
                component.confirmAssignmentOfChore();
            });

            it('should navigate to Child\'s Assigned Chores screen', () => {
                // Act
                component.confirmAssignmentOfChore();

                // Assert
                expect(router.navigate).toHaveBeenCalled();
                const expectedPath = ['/ui', 'child', `${expectedChildId}`, 'chores'];
                expect(router.navigate).toHaveBeenCalledWith(expectedPath);
            });
        });

        describe('and failure,', () => {
            const choreId = Math.trunc(Math.random() * 1000);

            beforeEach(() => {
                // Arrange
                if (!component.selectedChore) {
                    fail('Selected chore should be set');
                    return;
                }
                component.selectedChore.id = choreId;
                component.assignChoreConfirmation = true;

                choreService.assignChore.and.returnValue(throwError({}));
            });

            it('should leave assignChoreConfirmation as true', () => {
                // Act
                component.confirmAssignmentOfChore();

                // Assert
                expect(component.assignChoreConfirmation).toEqual(true);
            });

            it('should not navigate', () => {
                // Act
                component.confirmAssignmentOfChore();

                // Assert
                expect(router.navigate).not.toHaveBeenCalled();
            });
        });
    });

    describe('navigateToAddChoreScreen', () => {
        it('should navigate to Assign Chore screen', () => {
            // Arrange
            const expectedChildId = 12345;
            const child: Child = {
                id: expectedChildId,
                firstName: '',
                email: '',
            };
            component.selectedChild = child;

            // Act
            component.navigateToAddChoreScreen();

            // Assert
            expect(router.navigate).toHaveBeenCalled();
            const expectedPath = ['/ui', 'child', `${expectedChildId}`, 'chores', 'add'];
            expect(router.navigate).toHaveBeenCalledWith(expectedPath);
        });
    });

    describe('onSelectChoreForAssignment', () => {
        it('given assignChoreConfirmation is false, sets it to true', () => {
            // Arrange
            component.assignChoreConfirmation = false;

            // Act
            component.onSelectChoreForAssignment({name: ''});

            // Assert
            expect(component.assignChoreConfirmation).toEqual(true);
        });

        it('given assignChoreConfirmation is true, sets it to false', () => {
            // Arrange
            component.assignChoreConfirmation = true;

            // Act
            component.onSelectChoreForAssignment({name: ''});

            // Assert
            expect(component.assignChoreConfirmation).toEqual(false);
        });

        it('sets selectedChore to chosen Chore', () => {
            // Arrange
            component.selectedChore = {name: ''};
            const expectedChoreName = 'Asdf_Chore_name_123';

            // Act
            component.onSelectChoreForAssignment({ name: expectedChoreName });

            // Assert
            expect(component.selectedChore).toEqual({ name: expectedChoreName });
        });
    });

});
