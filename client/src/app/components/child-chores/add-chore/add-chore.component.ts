import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-chore',
  templateUrl: './add-chore.component.html',
  styleUrls: ['./add-chore.component.css']
})
export class AddChoreComponent implements OnInit {
  choreName = '';
  childId: number | undefined;

  choreDuplicate = false;
  choreNameMissing = false;
  errorAddingChore = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private choreService: ChoreService,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.childId = params.id;
    });

    this.navigateBackService.push(null);
  }

  public addAvailableChore(): void {
    if (this.choreName) {
      this.choreService.addAvailableChore(this.choreName).subscribe(() => {
        let path = environment.paths.child.navigation.prefix.slice();
        path.push(`${this.childId}`);
        path = path.concat(environment.paths.child.chores.assign.navigation.suffix.slice());
        this.router.navigate(path);
      }, (error) => {
        if (error.status === 409) {
          this.choreDuplicate = true;
          this.errorAddingChore = false;
        } else {
          this.choreDuplicate = false;
          this.errorAddingChore = true;
        }
      });
    } else {
      this.choreNameMissing = true;
    }
  }

  public navigateToAssignChoreScreen(): void {
    let path = environment.paths.child.navigation.prefix.slice();
    path.push(`${this.childId}`);
    path = path.concat(environment.paths.child.chores.assign.navigation.suffix.slice());
    this.router.navigate(path);
  }

}
