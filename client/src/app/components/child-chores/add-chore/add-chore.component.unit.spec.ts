import { of, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { fakeAsync, flush } from '@angular/core/testing';

import { AddChoreComponent } from './add-chore.component';
import { Chore } from 'src/app/models/chore';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { Child } from 'src/app/models/child';

describe('AddChoreComponent (unit)', () => {
    let component: AddChoreComponent;
    let mockRouteSnapshot: jasmine.SpyObj<ActivatedRoute>;
    let router: jasmine.SpyObj<Router>;
    let choreService: jasmine.SpyObj<ChoreService>;
    let navigateBackService: jasmine.SpyObj<NavigateBackService>;

    beforeEach(() => {
        mockRouteSnapshot = jasmine.createSpyObj('ActivatedRoute', ['']);

        router = jasmine.createSpyObj('Router', ['navigate']);

        choreService = jasmine.createSpyObj('ChoreService', ['addAvailableChore']);

        navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

        component = new AddChoreComponent(mockRouteSnapshot, router, choreService, navigateBackService);
    });

    describe('After initializing', () => {

        it('should have choreDuplicate set to false', () => {
            // Assert
            expect(component.choreDuplicate).toEqual(false);
        });

        it('should have choreNameMissing set to false', () => {
            // Assert
            expect(component.choreNameMissing).toEqual(false);
        });

        it('should have errorAddingChore set to false', () => {
            // Assert
            expect(component.errorAddingChore).toEqual(false);
        });

        it('should set choreName to empty Chore', () => {
            // Assert
            const expected = '';
            expect(component.choreName).toEqual(expected);
        });
    });

    describe('ngOnInit', () => {
        const expectedChildId = 5678;

        beforeEach(() => {
            const child: Child = {
                id: expectedChildId,
                firstName: '',
                email: ''
            };
            // Arrange
            mockRouteSnapshot.params = of(child);
        });

        it('should get Child\'s ID from path parameters', () => {
            // Act
            component.ngOnInit();

            // Assert
            expect(component.childId).toEqual(expectedChildId);
        });

        it('should push null call back to NavigateBackService', () => {
            // Act
            component.ngOnInit();

            // Assert
            expect(navigateBackService.push).toHaveBeenCalled();
            expect(navigateBackService.push).toHaveBeenCalledTimes(1);
            expect(navigateBackService.push).toHaveBeenCalledWith(null);
        });
    });

    describe('addAvailableChore,', () => {

        beforeEach(() => {
        });

        describe('given valid inputs,', () => {
            const expectedChoreName = 'Abcd 1234';

            beforeEach(() => {
                component.choreName = expectedChoreName;
                choreService.addAvailableChore.and.returnValue(of(void 0));
            });

            it('should call addAvailableChore on ChoreService', () => {
                component.addAvailableChore();

                expect(choreService.addAvailableChore).toHaveBeenCalled();
            });

            it('should call addAvailableChore on ChoreService, with correct parameters', () => {
                component.addAvailableChore();

                expect(choreService.addAvailableChore).toHaveBeenCalledWith(expectedChoreName);
            });

            it('should subscribe to result from addAvailableChore on ChoreService', () => {
                const result = jasmine.createSpyObj('Observable<void>', ['subscribe']);
                // Arrange
                choreService.addAvailableChore.and.returnValue(result);

                // Act
                component.addAvailableChore();

                expect(result.subscribe).toHaveBeenCalled();
            });

            describe('and successful addAvailableChore,', () => {
                const expectedChildId = Math.trunc(Math.random() * 1000);

                beforeEach(() => {
                    // Arrange
                    component.childId = expectedChildId;
                    choreService.addAvailableChore.and.returnValue(of(void 0));

                });

                it('should navigate to the Assign Chore screen', () => {
                    // Act
                    component.addAvailableChore();
                    expect(router.navigate).toHaveBeenCalled();
                    expect(router.navigate).toHaveBeenCalledTimes(1);
                    expect(router.navigate).toHaveBeenCalledWith(['/ui', 'child', `${expectedChildId}`, 'chores', 'assign']);
                });
            });
        });

        describe('given Chore name undefined,', () => {

            beforeEach(() => {
                component.choreName = '';
                component.choreNameMissing = false;

                component.addAvailableChore();
            });

            it('should not attempt to create the chore entity', () => {
                expect(choreService.addAvailableChore).not.toHaveBeenCalled();
            });

            it('should set firstName as missing', () => {
                expect(component.choreNameMissing).toBe(true);
            });
        });

        describe('given Chore name empty,', () => {

            beforeEach(() => {
                component.choreName = '';
                component.choreNameMissing = false;

                component.addAvailableChore();
            });

            it('should not attempt to create the chore entity', () => {
                expect(choreService.addAvailableChore).not.toHaveBeenCalled();
            });

            it('should set firstName as missing', () => {
                expect(component.choreNameMissing).toBe(true);
            });
        });

        describe('given valid inputs and unsuccessful addAvailableChore,', () => {
            it('should not navigate to the Assign Chore screen', fakeAsync(() => {
                component.choreName = 'asdf1234';

                choreService.addAvailableChore.and.returnValue(throwError({}));

                component.addAvailableChore();

                expect(router.navigate).not.toHaveBeenCalled();
                flush();
            }));

            it('should still indicate that we are not assigning chores', fakeAsync(() => {
                component.choreName = 'asdf1234';

                choreService.addAvailableChore.and.returnValue(throwError({}));

                component.addAvailableChore();

                expect(router.navigate).not.toHaveBeenCalled();
                flush();
            }));

            describe('error with adding duplicate Chore,', () => {
                beforeEach(() => {
                    // Arrange
                    component.choreName = 'asdf1234';
                    component.choreDuplicate = false;
                    component.errorAddingChore = true;
                    const duplicateErrResponse = {
                        status: 409,
                        message: 'Chore already exists'
                    };

                    choreService.addAvailableChore.and.returnValue(throwError(duplicateErrResponse));

                    // Act
                    component.addAvailableChore();
                });

                it('should set duplicate chore flag to true', fakeAsync(() => {
                    // Assert
                    expect(component.choreDuplicate).toEqual(true);
                    flush();
                }));

                it('should set generic error flag to false', fakeAsync(() => {
                    // Assert
                    expect(component.errorAddingChore).toEqual(false);
                    flush();
                }));
            });

            describe('generic error with adding Child,', () => {
                beforeEach(() => {
                    // Arrange
                    component.choreName = 'asdf1234';
                    component.choreDuplicate = true;
                    component.errorAddingChore = false;

                    choreService.addAvailableChore.and.returnValue(throwError({}));

                    // Act
                    component.addAvailableChore();
                });

                it('should set generic error flag to true', fakeAsync(() => {
                    // Assert
                    expect(component.errorAddingChore).toEqual(true);
                    flush();
                }));

                it('should set duplicate chore flag to false', fakeAsync(() => {
                    // Assert
                    expect(component.choreDuplicate).toEqual(false);
                    flush();
                }));
            });

        });

    });

    describe('navigateToAssignChoreScreen', () => {

        it(`should navigate to Assign Chores screen`, () => {
            const expectedChildId = Math.trunc(Math.random() * 1000);
            // Arrange
            component.childId = expectedChildId;
            // Act
            component.navigateToAssignChoreScreen();

            // Assert
            expect(router.navigate).toHaveBeenCalled();
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/ui', 'child', `${expectedChildId}`, 'chores', 'assign']);
        });
    });
});
