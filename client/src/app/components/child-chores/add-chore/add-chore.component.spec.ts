import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { AddChoreComponent } from './add-chore.component';
import { ChoreService } from 'src/app/services/chore.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

describe('AddChoreComponent', () => {
  let component: AddChoreComponent;
  let fixture: ComponentFixture<AddChoreComponent>;

  beforeEach(waitForAsync(() => {
    const mockActivatedRoute = jasmine.createSpyObj('ActivatedRoute', ['']);
    mockActivatedRoute.params = of({id: 1});

    const mockRouter = jasmine.createSpyObj('Router', ['']);
    const mockChoreService = jasmine.createSpyObj('ChoreService', ['']);
    const mockNavigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

    TestBed.configureTestingModule({
      declarations: [AddChoreComponent],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: Router, useValue: mockRouter },
        { provide: ChoreService, useValue: mockChoreService },
        { provide: NavigateBackService, useValue: mockNavigateBackService },
      ],
      imports: [
        FormsModule,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
