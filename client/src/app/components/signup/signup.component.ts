import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { SignupService } from 'src/app/services/signup.service';

const EMAIL_REGEX = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

const SIGNIN_PATH = environment.paths.signIn.route;
const EMAIL_CONFIRMATION_PATH = environment.paths.emailConfirmation;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  email = '';
  password = '';
  passwordConfirmation = '';
  public passwordMismatch = false;
  public emailInvalid = false;
  public emailDuplicate = false;
  public loginPath = SIGNIN_PATH;

  constructor(
    private signupService: SignupService,
    private router: Router,
    ) { }

  signUp(): void {
    const emailValid = this.email && this.email !== '' && this.email.match(EMAIL_REGEX) !== null;
    const passwordsEntered = this.password && this.password !== '' && this.passwordConfirmation && this.passwordConfirmation !== '';
    const passwordsMatch = this.password === this.passwordConfirmation;
    if (emailValid && passwordsEntered && passwordsMatch) {
      this.signupService.signup(this.email, this.password).subscribe(() => {
        this.router.navigate([`/${EMAIL_CONFIRMATION_PATH}`]);
      }, (response) => {
        this.emailDuplicate = true;
      });
    } else if (!passwordsMatch) {
      this.passwordMismatch = true;
    } else if (!emailValid) {
      this.emailInvalid = true;
    }
  }

}
