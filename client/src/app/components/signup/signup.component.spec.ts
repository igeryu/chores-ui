import {
  ComponentFixture,
  TestBed,
  waitForAsync
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { SignupComponent } from './signup.component';
import { SignupService } from 'src/app/services/signup.service';

describe('SignupComponent', () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(waitForAsync(() => {
    const mockSignupService = jasmine.createSpyObj('SignupService', ['']);

    TestBed.configureTestingModule({
      providers: [
        { provide: SignupService, useValue: mockSignupService },
      ],
      declarations: [SignupComponent],
      imports: [
        FormsModule,
        RouterTestingModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a header', () => {
    const header = fixture.debugElement.query(By.css('h1'));

    expect(header).toBeTruthy();
  });

  it('should header of "Sign Up"', () => {
    const header = fixture.debugElement.query(By.css('h1')).nativeElement;

    expect(header.textContent).toEqual('Sign Up');
  });

  describe('should have inputs:', () => {
    it('email', () => {
      const emailInput = fixture.debugElement.query(By.css('[data-cy=email]'));

      expect(emailInput).toBeTruthy();
    });

    it('password', () => {
      const passwordInput = fixture.debugElement.query(By.css('[data-cy=password]'));

      expect(passwordInput).toBeTruthy();
    });

    it('password confirmation', () => {
      const passwordConfirmationInput = fixture.debugElement.query(By.css('[data-cy=matchingPassword]'));

      expect(passwordConfirmationInput).toBeTruthy();
    });

    it('submit button', () => {
      const submitButton = fixture.debugElement.query(By.css('[data-cy=submit]'));

      expect(submitButton).toBeTruthy();
    });
  });

  describe('should have error messages, when input is invalid:', () => {
    it('invalid email', () => {
      component.emailInvalid = true;
      fixture.detectChanges();

      const errorDE = fixture.debugElement.query(By.css('div.error'));
      expect(errorDE).toBeTruthy();

      const error = errorDE.nativeElement;
      expect(error.textContent).toEqual('Email Invalid');
    });

    it('password mismatch', () => {
      component.passwordMismatch = true;
      fixture.detectChanges();

      const errorDE = fixture.debugElement.query(By.css('div.error'));
      expect(errorDE).toBeTruthy();

      const error = errorDE.nativeElement;
      expect(error.textContent).toEqual('Password Mismatch');
    });

    it('email duplicate', () => {
      component.emailDuplicate = true;
      fixture.detectChanges();

      const errorDE = fixture.debugElement.query(By.css('div.error'));
      expect(errorDE).toBeTruthy();

      const error = errorDE.nativeElement;
      expect(error.textContent).toEqual('Account Already Exists');
    });
  });

  describe('should not have error messages, when input is valid:', () => {
    it('valid email', () => {
      component.emailInvalid = false;
      fixture.detectChanges();

      const errorDE = fixture.debugElement.query(By.css('div.error'));
      expect(errorDE).toBeFalsy();
    });

    it('passwords match', () => {
      component.passwordMismatch = false;
      fixture.detectChanges();

      const errorDE = fixture.debugElement.query(By.css('div.error'));
      expect(errorDE).toBeFalsy();
    });
  });
});
