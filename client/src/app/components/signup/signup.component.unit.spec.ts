import { flush, fakeAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import {
  of,
  throwError
} from 'rxjs';

import { environment } from 'src/environments/environment';

import { SignupComponent } from './signup.component';

describe('SignupComponent (unit)', () => {
  let component: SignupComponent;
  let router: Router;
  let signupService: any;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);
    signupService = jasmine.createSpyObj('SignupService', ['signup']);
    component = new SignupComponent(signupService, router);
  });

  describe('on initialization', () => {

    it('should have passwordMismatch error flag set to false', () => {
      expect(component.passwordMismatch).toEqual(false);
    });

    it('should have emailInvalid error flag set to false', () => {
      expect(component.emailInvalid).toEqual(false);
    });

    it('should have emailDuplicate error flag set to false', () => {
      expect(component.emailDuplicate).toEqual(false);
    });
  });

  describe('signUp,', () => {
    it('given valid inputs, should attempt to sign the user up', () => {
      component.email = 'example@example.com';
      component.password = 'asdf1234';
      component.passwordConfirmation = 'asdf1234';
      signupService.signup.and.returnValue(of({}));

      component.signUp();

      expect(signupService.signup).toHaveBeenCalled();
      expect(signupService.signup).toHaveBeenCalledWith('example@example.com', 'asdf1234');
    });

    it('given valid inputs and successful signup, should navigate to email confirmation page', () => {
      component.email = 'example@example.com';
      component.password = 'asdf1234';
      component.passwordConfirmation = 'asdf1234';
      signupService.signup.and.returnValue(of({}));

      component.signUp();

      expect(router.navigate).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith([`/${environment.paths.emailConfirmation}`]);
    });

    it('given invalid email, should not attempt to sign the user up', () => {
      component.email = 'xyz123';
      component.password = 'asdf';
      component.passwordConfirmation = 'asdf';

      component.signUp();

      expect(signupService.signup).not.toHaveBeenCalled();
    });

    it('given invalid email, should set email error flag', () => {
      component.email = 'xyz123';
      component.password = 'asdf';
      component.passwordConfirmation = 'asdf';
      component.emailInvalid = false;

      component.signUp();

      expect(component.emailInvalid).toEqual(true);
    });

    it('given all inputs empty, should not attempt to sign the user up', () => {
      component.email = '';
      component.password = '';
      component.passwordConfirmation = '';

      component.signUp();

      expect(signupService.signup).not.toHaveBeenCalled();
    });

    it('given first password empty, should not attempt to sign the user up', () => {
      component.email = 'example@example.com';
      component.password = '';
      component.passwordConfirmation = 'asdf';

      component.signUp();

      expect(signupService.signup).not.toHaveBeenCalled();
    });

    it('given confirmation password empty, should not attempt to sign the user up', () => {
      component.email = 'example@example.com';
      component.password = 'asdf';
      component.passwordConfirmation = '';

      component.signUp();

      expect(signupService.signup).not.toHaveBeenCalled();
    });

    it('given passwords do not match, should not attempt to sign the user up', () => {
      component.email = 'example@example.com';
      component.password = 'asdf';
      component.passwordConfirmation = '1234';

      component.signUp();

      expect(signupService.signup).not.toHaveBeenCalled();
    });

    it('given passwords do not match, should set password mismatch error flag', () => {
      component.email = 'example@example.com';
      component.password = 'asdf';
      component.passwordConfirmation = '1234';
      component.passwordMismatch = false;

      component.signUp();

      expect(component.passwordMismatch).toEqual(true);
    });

    describe('given valid inputs and unsuccessful signup,', () => {

      beforeEach(() => {
        component.email = 'example@example.com';
        component.password = 'asdf1234';
        component.passwordConfirmation = 'asdf1234';
      });

      it('should not navigate to email confirmation page', fakeAsync(() => {
        signupService.signup.and.returnValue(throwError({}));

        component.signUp();

        expect(router.navigate).not.toHaveBeenCalled();
        flush();
      }));

      it('and response has HTTP Status 409, should set Account Exists error flag', fakeAsync(() => {
        // Arrange
        const duplicateErrResponse = {
          status: 409,
          message: 'Account already exists'
        };
        signupService.signup.and.returnValue(throwError(duplicateErrResponse));
        component.emailDuplicate = false;


        // Act
        component.signUp();

        // Assert
        expect(component.emailDuplicate).toEqual(true);
        flush();
      }));

    });
  });
});
