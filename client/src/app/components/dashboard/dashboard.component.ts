import {
  Component,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { ChildService } from 'src/app/services/child.service';
import { Child } from 'src/app/models/child';
import { NavigateBackService } from 'src/app/services/navigate-back.service';
import { Observable, of } from 'rxjs';

const EMAIL_REGEX = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public addingChild = false;
  public childAdded = false;
  public email = '';
  public firstName = '';
  public children: Child[] = [];

  // Errors
  public firstNameMissing = false;
  public emailMissing = false;
  public emailInvalid = false;
  public errorAddingChild = false;
  public childDuplicate = false;

  constructor(
    private childService: ChildService,
    private router: Router,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.navigateBackService.push(null);
    this.updateChildrenList();
  }

  public toggleAddChildDisplay(): void {
    this.addingChild = !this.addingChild;
    this.firstNameMissing = false;
    this.emailMissing = false;
    this.emailInvalid = false;
    this.errorAddingChild = false;
    this.firstName = '';
    this.email = '';
    this.navigateBackService.push(null);
  }

  public addChild(): void {
    if (this.email && this.email.match(EMAIL_REGEX) && this.firstName) {
      this.childService.addChild(this.email, this.firstName).subscribe(() => {
        this.addingChild = false;
        this.childAdded = true;
        this.firstNameMissing = false;
        this.errorAddingChild = false;
        this.updateChildrenList();
      }, (response) => {
        if (response.status === 409) {
          this.childDuplicate = true;
        } else {
          this.errorAddingChild = true;
        }
      });
    } else {
      if (!this.email) {
        this.emailMissing = true;
      } else if (!this.email.match(EMAIL_REGEX)) {
        this.emailInvalid = true;
      }
      if (!this.firstName) {
        this.firstNameMissing = true;
      }

    }
  }

  public doneAddingChild(): void {
    this.addingChild = false;
    this.childAdded = false;
  }

  public selectChild(childId: number): void {
    let path = environment.paths.child.navigation.prefix.slice();
    path.push(`${childId}`);
    path = path.concat(environment.paths.child.chores.assigned.navigation.suffix.slice());
    this.router.navigate(path);
  }

  private updateChildrenList(): void {
    this.childService.getChildren().subscribe((childrenResponse: Child[]) => {
      this.children = childrenResponse;
    });
  }

}
