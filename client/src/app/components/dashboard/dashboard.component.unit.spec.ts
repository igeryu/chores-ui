import {
  of,
  throwError,
} from 'rxjs';
import {
  fakeAsync,
  flush,
} from '@angular/core/testing';
import { Router } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { Child } from 'src/app/models/child';
import { ChildService } from 'src/app/services/child.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

describe('DashboardComponent (unit)', () => {
  let component: DashboardComponent;
  let childService: jasmine.SpyObj<ChildService>;
  let router: Router;
  let navigateBackService: jasmine.SpyObj<NavigateBackService>;

  beforeEach(() => {
    childService = jasmine.createSpyObj('ChildService', [
      'addChild',
      'getChildren',
    ]);
    router = jasmine.createSpyObj('Router', ['navigate']);
    navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

    component = new DashboardComponent(childService, router, navigateBackService);
  });

  describe('after initializing,', () => {

    it('should have addingChild set to false', () => {
      expect(component.addingChild).toEqual(false);
    });

    it('should have childAdded set to false', () => {
      expect(component.childAdded).toEqual(false);
    });

    it('should have firstNameMissing set to false', () => {
      expect(component.firstNameMissing).toEqual(false);
    });

    it('should have emailMissing set to false', () => {
      expect(component.emailMissing).toEqual(false);
    });

    it('should have emailInvalid set to false', () => {
      expect(component.emailInvalid).toEqual(false);
    });

    it('should have errorAddingChild set to false', () => {
      expect(component.errorAddingChild).toEqual(false);
    });

    it('should have childDuplicate set to false', () => {
      expect(component.childDuplicate).toEqual(false);
    });

    it('should have an empty children list', () => {
      expect(component.children).toEqual([]);
    });
  });

  describe('ngOnInit', () => {

    it(`should push null call back to NavigateBackService`, () => {
      // Arrange
      childService.getChildren.and.returnValue(of([]));

      // Act
      component.ngOnInit();

      // Assert
      expect(navigateBackService.push).toHaveBeenCalled();
      expect(navigateBackService.push).toHaveBeenCalledTimes(1);
      expect(navigateBackService.push).toHaveBeenCalledWith(null);
    });

    it('should update list of Children', () => {
      // Arrange
      component.children = [];
      const expectedChildren = [
        { firstName: 'asdf_1234' },
        { firstName: '1234_asf' },
      ] as Child[];
      childService.getChildren.and.returnValue(of(expectedChildren));

      // Act
      component.ngOnInit();

      // Assert
      expect(component.children.length).toEqual(expectedChildren.length);
      for (let x = 0; x < expectedChildren.length; x++) {
        expect(component.children[x]).toEqual(expectedChildren[x]);
      }
    });
  });

  describe('toggleAddChildDisplay', () => {

    [
      { input: true, expected: false },
      { input: false, expected: true },
    ].forEach(testCase => {
      describe(`given addingChild is ${testCase.input}`, () => {
        beforeEach(() => {
          component.addingChild = testCase.input;
        });

        it(`should change addingChild from ${testCase.input} to ${testCase.expected}`, () => {
          component.toggleAddChildDisplay();

          expect(component.addingChild).toEqual(testCase.expected);
        });

        const expectedCallBack = testCase.input ? 'null' : 'a "display Add Child"';

        it(`should push ${expectedCallBack} call back to NavigateBackService`, () => {
          // Act
          component.toggleAddChildDisplay();

          // Assert
          expect(navigateBackService.push).toHaveBeenCalled();
          expect(navigateBackService.push).toHaveBeenCalledTimes(1);
          expect(navigateBackService.push).toHaveBeenCalledWith(null);
        });
      });
    });

    describe('should reset', () => {
      beforeEach(() => {
        component.firstNameMissing = true;
        component.emailMissing = true;
        component.emailInvalid = true;
        component.errorAddingChild = true;
        component.firstName = 'ljsdl';
        component.email = 'ljsdl';

        component.toggleAddChildDisplay();
      });

      it(`missing firstName flag`, () => {
        expect(component.firstNameMissing).toEqual(false);
      });

      it(`missing email flag`, () => {
        expect(component.emailMissing).toEqual(false);
      });

      it(`invalid email flag`, () => {
        expect(component.emailInvalid).toEqual(false);
      });

      it(`Add Child error flag`, () => {
        expect(component.errorAddingChild).toEqual(false);
      });

      it(`first name input`, () => {
        expect(component.firstName).toEqual('');
      });

      it(`email input`, () => {
        expect(component.email).toEqual('');
      });
    });

  });

  describe('addChild', () => {

    beforeEach(() => {
      component.addingChild = true;
      component.childAdded = false;
    });

    describe('given valid inputs,', () => {
      const expectedEmail = 'asdf@email.com';
      const expectedFirstName = 'asdf_first_name';

      beforeEach(() => {
        component.email = expectedEmail;
        component.firstName = expectedFirstName;
        component.children = [];
        childService.addChild.and.returnValue(of(void 0));
        childService.getChildren.and.returnValue(of([]));
      });

      it('should call addChild on ChildService', () => {
        component.addChild();

        expect(childService.addChild).toHaveBeenCalled();
      });

      it('should call addChild on ChildService, with correct parameters', () => {
        component.addChild();

        expect(childService.addChild).toHaveBeenCalledWith(expectedEmail, expectedFirstName);
      });

      describe('and successful addChild,', () => {
        const expectedChildList: Child[] = [
          { id: 1234, firstName: 'Child_abcd', email: '' },
        ];

        beforeEach(() => {
          childService.getChildren.and.returnValue(of(expectedChildList));
          component.addChild();
        });

        it('should set addingChild to false', () => {
          expect(component.addingChild).toEqual(false);
        });

        it('should set childAdded to true', () => {
          expect(component.childAdded).toEqual(true);
        });

        it('should set firstNameMissing to false', () => {
          expect(component.firstNameMissing).toEqual(false);
        });

        it('should set emailMissing to false', () => {
          expect(component.emailMissing).toEqual(false);
        });

        it('should set emailInvalid to false', () => {
          expect(component.emailInvalid).toEqual(false);
        });

        it('should set errorAddingChild to false', () => {
          expect(component.errorAddingChild).toEqual(false);
        });

        it('should update the Child list', () => {
          expect(component.children).toEqual(expectedChildList);
        });
      });
    });

    describe('given all inputs empty,', () => {
      beforeEach(() => {
        component.email = '';
        component.firstName = '';
        component.firstNameMissing = false;
        component.emailMissing = false;

        component.addChild();
      });

      it('should not attempt to create the child entity', () => {
        expect(childService.addChild).not.toHaveBeenCalled();
      });

      it('should set firstName as missing', () => {
        expect(component.firstNameMissing).toBe(true);
      });

      it('should set email as missing', () => {
        expect(component.emailMissing).toBe(true);
      });

      it('should set email as not invalid', () => {
        expect(component.emailInvalid).toBe(false);
      });
    });

    describe('given firstName empty,', () => {
      beforeEach(() => {
        component.email = 'example@example.com';
        component.firstName = '';
        component.firstNameMissing = false;
        component.emailMissing = false;

        component.addChild();
      });

      it('should not attempt to create the child entity', () => {
        expect(childService.addChild).not.toHaveBeenCalled();
      });

      it('should set firstName as missing', () => {
        expect(component.firstNameMissing).toBe(true);
      });

      it('should set email as not missing', () => {
        expect(component.emailMissing).toBe(false);
      });

      it('should set email as not invalid', () => {
        expect(component.emailInvalid).toBe(false);
      });
    });

    describe('given email invalid and first name present,', () => {
      beforeEach(() => {
        component.email = 'abcd_not_valid';
        component.firstName = 'asdf';

        component.addChild();
      });

      it('should not attempt to create the child entity', () => {
        expect(childService.addChild).not.toHaveBeenCalled();
      });

      it('should set firstName as not missing', () => {
        expect(component.firstNameMissing).toBe(false);
      });

      it('should set email as not missing', () => {
        expect(component.emailMissing).toBe(false);
      });

      it('should set email as invalid', () => {
        expect(component.emailInvalid).toBe(true);
      });
    });

    describe('given email invalid and first name is empty,', () => {
      beforeEach(() => {
        component.email = 'abcd_not_valid';
        component.firstName = '';

        component.addChild();
      });

      it('should not attempt to create the child entity', () => {
        expect(childService.addChild).not.toHaveBeenCalled();
      });

      it('should set firstName as not missing', () => {
        expect(component.firstNameMissing).toBe(true);
      });

      it('should set email as not missing', () => {
        expect(component.emailMissing).toBe(false);
      });

      it('should set email as invalid', () => {
        expect(component.emailInvalid).toBe(true);
      });
    });

    describe('given valid inputs and unsuccessful addChild,', () => {
      it('should not display dashboard again', fakeAsync(() => {
        component.email = 'example@example.com';
        component.firstName = 'asdf1234';

        childService.addChild.and.returnValue(throwError({}));

        component.addChild();

        expect(component.addingChild).toEqual(true);
        flush();
      }));

      it('should not indicate the child was added', fakeAsync(() => {
        component.email = 'example@example.com';
        component.firstName = 'asdf1234';

        childService.addChild.and.returnValue(throwError({}));

        component.addChild();

        expect(component.childAdded).toEqual(false);
        flush();
      }));

      it('should not have any new children in the list', fakeAsync(() => {
        component.email = 'example@example.com';
        component.firstName = 'asdf1234';
        component.children = [];

        childService.addChild.and.returnValue(throwError({}));

        component.addChild();

        expect(component.children).toEqual([]);
        flush();
      }));

      describe('should indicate', () => {

        beforeEach(() => {
          component.email = 'example@example.com';
          component.firstName = 'asdf1234';
        });

        it('generic error with adding Child', fakeAsync(() => {
          // Arrange
          component.errorAddingChild = false;

          childService.addChild.and.returnValue(throwError({}));

          // Act
          component.addChild();

          // Assert
          expect(component.errorAddingChild).toEqual(true);
          flush();
        }));

        it('error with adding duplicate Child', fakeAsync(() => {
          component.childDuplicate = false;
          const duplicateErrResponse = {
            status: 409,
            message: 'Account already exists'
          };

          childService.addChild.and.returnValue(throwError(duplicateErrResponse));

          component.addChild();

          expect(component.childDuplicate).toEqual(true);
          flush();
        }));

      });
    });

  });

  describe('doneAddingChild,', () => {
    [
      { input: true, expected: false },
      { input: false, expected: false },
    ].forEach(testCase => {
      it(`given addingChild is ${testCase.input}, should be ${testCase.expected} after call`, () => {
        component.addingChild = testCase.input;

        component.doneAddingChild();

        expect(component.addingChild).toEqual(testCase.expected);
      });

      it(`given childAdded is ${testCase.input}, should be ${testCase.expected} after call`, () => {
        component.childAdded = testCase.input;

        component.doneAddingChild();

        expect(component.childAdded).toEqual(testCase.expected);
      });
    });
  });

  describe('selectChild,', () => {

    it('should navigate to Child\'s Assigned Chores screen', () => {
      // Arrange
      const expectedChildId = 12345;

      // Act
      component.selectChild(expectedChildId);

      // Assert
      expect(router.navigate).toHaveBeenCalled();
      const expectedPath = ['/ui', 'child', `${expectedChildId}`, 'chores'];
      expect(router.navigate).toHaveBeenCalledWith(expectedPath);
    });
  });
});
