import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { DashboardComponent } from './dashboard.component';
import { ChildService } from 'src/app/services/child.service';
import { By } from '@angular/platform-browser';
import { Child } from 'src/app/models/child';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(waitForAsync(() => {
    const mockChildService = jasmine.createSpyObj('ChildService', ['getChildren']);
    mockChildService.getChildren.and.returnValue(of([] as Child[]));

    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [
        FormsModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: ChildService, useValue: mockChildService }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on the Children screen', () => {

    beforeEach(() => {
      component.childAdded = false;
      component.addingChild = false;
      fixture.detectChanges();
    });

    it('should have header "Children"', () => {
      const header = fixture.debugElement.query(By.css('h1')).nativeElement;

      expect(header).toBeTruthy();
      expect(header.textContent).toEqual('Children');
    });

    describe('Children list', () => {

      it('given no children, should not be visible', () => {
        component.children = [];
        fixture.detectChanges();

        const child = fixture.debugElement.query(By.css('.child'));

        expect(child).toBeFalsy();
      });

      describe('given one child,', () => {
        const expectedName = 'expected_name';
        const expectedChildId = 12345;

        beforeEach(() => {
          component.children = [{ id: expectedChildId, firstName: expectedName, email: '' }];
          fixture.detectChanges();
        });

        it('one child button should be visible', () => {
          const child = fixture.debugElement.query(By.css('.child'));

          expect(child).toBeTruthy();
          expect(child.nativeElement.textContent).toContain(expectedName);
        });

        it('clicking on the child should call selectChild method', () => {
          // Arrange
          const selectChild = spyOn(component, 'selectChild');
          const child = fixture.debugElement.query(By.css('.child'));

          // Act
          child.nativeElement.click();

          // Assert
          expect(selectChild).toHaveBeenCalled();
          expect(selectChild).toHaveBeenCalledWith(expectedChildId);
        });
      });

      it('given two children, two child buttons should be visible', () => {
        const expectedNameA = 'expected_name_asdf';
        const expectedNameB = 'expected_name_1234';
        component.children = [
          { id: 1, firstName: expectedNameA, email: '' } as Child,
          { id: 2, firstName: expectedNameB, email: '' } as Child,
        ];
        fixture.detectChanges();

        const child = fixture.debugElement.queryAll(By.css('.child'));

        expect(child).toBeTruthy();
        expect(child.length).toEqual(2);
        expect(child[0].nativeElement.textContent).toContain(expectedNameA);
        expect(child[1].nativeElement.textContent).toContain(expectedNameB);
      });

    });

    describe('add Child button,', () => {

      it('should be visible', () => {
        const addChildButton = fixture.debugElement.query(By.css('[data-cy=add-child]'));

        expect(addChildButton).toBeTruthy();
      });

      it('should call addChild method', () => {
        const toggleAddChildDisplay = spyOn(component, 'toggleAddChildDisplay');
        const addChildButton = fixture.debugElement.query(By.css('[data-cy=add-child]')).nativeElement;

        addChildButton.click();

        expect(toggleAddChildDisplay).toHaveBeenCalled();
      });

    });

  });

  describe('on the Add Child screen,', () => {
    beforeEach(() => {
      component.addingChild = true;
      fixture.detectChanges();
    });

    it('should have header "Add Child"', () => {
      const header = fixture.debugElement.query(By.css('h1')).nativeElement;

      expect(header).toBeTruthy();
      expect(header.textContent).toEqual('Add Child');
    });

    describe('should have inputs:', () => {

      it('first name', () => {
        const firstNameInput = fixture.debugElement.query(By.css('[data-cy=first-name]'));

        expect(firstNameInput).toBeTruthy();
      });

      it('email', () => {
        const emailInput = fixture.debugElement.query(By.css('[data-cy=email]'));

        expect(emailInput).toBeTruthy();
      });

      describe('submit button,', () => {

        it('should be visible', () => {
          const submitButton = fixture.debugElement.query(By.css('[data-cy=submit]'));

          expect(submitButton).toBeTruthy();
        });

        it('should call addChild method', () => {
          const addChild = spyOn(component, 'addChild');
          const submitButton = fixture.debugElement.query(By.css('[data-cy=submit]')).nativeElement;

          submitButton.click();

          expect(addChild).toHaveBeenCalled();
        });

      });

      describe('cancel button,', () => {

        it('should be visible', () => {
          const cancelButton = fixture.debugElement.query(By.css('[data-cy=cancel]'));

          expect(cancelButton).toBeTruthy();
        });

        it('should call toggleAddChildDisplay method', () => {
          const toggleAddChildDisplay = spyOn(component, 'toggleAddChildDisplay');
          const cancelButton = fixture.debugElement.query(By.css('[data-cy=cancel]')).nativeElement;

          cancelButton.click();

          expect(toggleAddChildDisplay).toHaveBeenCalled();
        });

      });

    });

    describe('should show error messages:', () => {

      it('first name missing', () => {
        component.firstNameMissing = true;
        fixture.detectChanges();

        const errorDE = fixture.debugElement.query(By.css('div.error'));
        expect(errorDE).toBeTruthy();

        const error = errorDE.nativeElement;
        expect(error.textContent).toEqual('First Name Missing');
      });

      it('email missing', () => {
        component.emailMissing = true;
        fixture.detectChanges();

        const errorDE = fixture.debugElement.query(By.css('div.error'));
        expect(errorDE).toBeTruthy();

        const error = errorDE.nativeElement;
        expect(error.textContent).toEqual('Email Missing');
      });

      it('email invalid', () => {
        component.emailInvalid = true;
        fixture.detectChanges();

        const errorDE = fixture.debugElement.query(By.css('div.error'));
        expect(errorDE).toBeTruthy();

        const error = errorDE.nativeElement;
        expect(error.textContent).toEqual('Email Invalid');
      });

      it('generic error when adding child', () => {
        component.errorAddingChild = true;
        fixture.detectChanges();

        const errorDE = fixture.debugElement.query(By.css('div.error'));
        expect(errorDE).toBeTruthy();

        const error = errorDE.nativeElement;
        expect(error.textContent).toEqual('Failed to add child');
      });

      it('child duplicate', () => {
        component.childDuplicate = true;
        fixture.detectChanges();

        const errorDE = fixture.debugElement.query(By.css('div.error'));
        expect(errorDE).toBeTruthy();

        const error = errorDE.nativeElement;
        expect(error.textContent).toEqual('Child\'s email already used');
      });

    });
  });

  describe('on the Child Added screen,', () => {
    beforeEach(() => {
      component.addingChild = false;
      component.childAdded = true;
      fixture.detectChanges();
    });

    it('should have header "Child Added"', () => {
      const header = fixture.debugElement.query(By.css('h1')).nativeElement;

      expect(header).toBeTruthy();
      expect(header.textContent).toEqual('Child Added');
    });

    it('should have proper sub-header, as paragraph', () => {
      const paragraph = fixture.debugElement.query(By.css('p')).nativeElement;

      expect(paragraph).toBeTruthy();
      expect(paragraph.textContent).toEqual('Please have your child check their email for an account confirmation message');
    });


    describe('done button,', () => {

      it('should be visible', () => {
        const doneButton = fixture.debugElement.query(By.css('[data-cy=done]'));

        expect(doneButton).toBeTruthy();
      });

      it('should call addChild method', () => {
        const doneAddingChild = spyOn(component, 'doneAddingChild');
        const doneButton = fixture.debugElement.query(By.css('[data-cy=done]')).nativeElement;

        doneButton.click();

        expect(doneAddingChild).toHaveBeenCalled();
      });

    });


  });
});
