import {
  ComponentFixture,
  TestBed,
  waitForAsync
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AuthService } from 'src/app/services/security/auth.service';
import { SignInComponent } from './sign-in.component';
import { SignInService } from 'src/app/services/sign-in.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SigninComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;

  beforeEach(waitForAsync(() => {
    const mockSigninService = jasmine.createSpyObj('SigninService', ['']);
    const mockAuthService = jasmine.createSpyObj('AuthService', ['']);

    TestBed.configureTestingModule({
      providers: [
        { provide: SignInService, useValue: mockSigninService },
        { provide: AuthService, useValue: mockAuthService },
      ],
      declarations: [ SignInComponent ],
      imports: [
         FormsModule,
         RouterTestingModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
