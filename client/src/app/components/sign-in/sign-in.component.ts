import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { AuthService } from 'src/app/services/security/auth.service';
import { SignInService } from 'src/app/services/sign-in.service';
import { OauthResponse } from 'src/app/models/oauth-response';
import { User } from 'src/app/models/user';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

const SIGNUP_NAVIGATION = environment.paths.signUp.route;
const DASHBOARD_NAVIGATION = environment.paths.dashboard.navigation;

const PREFIX_ROLE = 'ROLE_';

@Component({
  selector: 'app-signin',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  email = '';
  password = '';
  public signUpPath = SIGNUP_NAVIGATION;

  constructor(
    private signinService: SignInService,
    private router: Router,
    private authService: AuthService,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.navigateBackService.push(null);
  }

  public signIn(): void {
    if (this.email && this.password) {
      this.signinService.signin(this.email, this.password).subscribe((response: OauthResponse) => {
        const accessToken = response.access_token;
        this.authService.setAuthToken(accessToken);

        const encodedToken = accessToken.split('.')[1];
        const decoded = atob(encodedToken);
        const authorities = JSON.parse(decoded).authorities;
        const role = authorities[0];
        this.authService.setCurrentUser(
          { role: role.replace(PREFIX_ROLE, '') } as User
        );

        this.router.navigate(DASHBOARD_NAVIGATION);
      }, () => {
        // do nothing
      });
    }
  }

}
