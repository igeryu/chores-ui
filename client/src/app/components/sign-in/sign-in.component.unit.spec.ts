import { fakeAsync, flush } from '@angular/core/testing';
import { Router } from '@angular/router';
import {
  of,
  throwError
} from 'rxjs';

import { AuthService } from 'src/app/services/security/auth.service';
import { NavigateBackService } from 'src/app/services/navigate-back.service';
import { SignInComponent } from './sign-in.component';
import { SignInService } from 'src/app/services/sign-in.service';

import { environment } from 'src/environments/environment';

describe('SigninComponent (unit)', () => {
  let component: SignInComponent;
  let router: Router;
  let signinService: jasmine.SpyObj<SignInService>;
  let authService: AuthService;
  let navigateBackService: jasmine.SpyObj<NavigateBackService>;

  beforeEach(() => {
    router = jasmine.createSpyObj('Router', ['navigate']);
    signinService = jasmine.createSpyObj('SigninService', ['signin']);
    authService = jasmine.createSpyObj('AuthService', ['setAuthToken', 'setCurrentUser']);
    navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);

    component = new SignInComponent(signinService, router, authService, navigateBackService);
  });

  describe('ngOnInit', () => {

    it(`should push null call back to NavigateBackService`, (done) => {
      // Arrange
      navigateBackService.push.and.callFake((pushedValue) => {
        // Validating pushed function
        // Assert
        expect(pushedValue).toBeNull('Expected null callback to be pushed');
        done();
      });

      // Act
      component.ngOnInit();

      // Assert
      expect(navigateBackService.push).toHaveBeenCalled();
      expect(navigateBackService.push).toHaveBeenCalledTimes(1);
    });
  });

  describe('signIn,', () => {
    it('given valid inputs, should attempt to sign the user in', () => {
      component.email = 'example@example.com';
      component.password = 'asdf1234';
      const fakeToken = 'fake_encoded_header.' + btoa(JSON.stringify({ authorities: ['asdf'] })) + '.fake_encoded_signature';
      signinService.signin.and.returnValue(of({
        access_token: fakeToken
      }));

      component.signIn();

      expect(signinService.signin).toHaveBeenCalled();
      expect(signinService.signin).toHaveBeenCalledWith('example@example.com', 'asdf1234');
    });

    describe('given valid inputs and successful signin,', () => {

      it('should store the user\'s bearer token', () => {
        component.email = 'example@example.com';
        component.password = 'asdf1234';
        const expectedToken = 'fake_encoded_header.' + btoa(JSON.stringify({ authorities: ['asdf'] })) + '.fake_encoded_signature';
        signinService.signin.and.returnValue(of({
          access_token: expectedToken
        }));

        component.signIn();

        expect(authService.setAuthToken).toHaveBeenCalled();
        expect(authService.setAuthToken).toHaveBeenCalledWith(expectedToken);
      });

      it('should store the user\'s info', () => {
        component.email = 'example@example.com';
        component.password = 'asdf1234';
        const expectedRole = 'PROGRAMMER';
        const jwt = 'fake_encoded_header.' + btoa(JSON.stringify({ authorities: ['ROLE_' + expectedRole] })) + '.fake_encoded_signature';
        signinService.signin.and.returnValue(of({
          access_token: jwt
        }));

        component.signIn();

        expect(authService.setCurrentUser).toHaveBeenCalled();
        expect(authService.setCurrentUser).toHaveBeenCalledWith({ role: expectedRole });
      });

      it('should navigate to dashboard', () => {
        component.email = 'example@example.com';
        component.password = 'asdf1234';
        const fakeToken = 'fake_encoded_header.' + btoa(JSON.stringify({ authorities: ['asdf'] })) + '.fake_encoded_signature';
        signinService.signin.and.returnValue(of({
          access_token: fakeToken
        }));

        component.signIn();

        expect(router.navigate).toHaveBeenCalled();
        expect(router.navigate).toHaveBeenCalledWith(environment.paths.dashboard.navigation);
      });
    });

    it('given all inputs empty, should not attempt to sign the user in', () => {
      component.email = '';
      component.password = '';

      component.signIn();

      expect(signinService.signin).not.toHaveBeenCalled();
    });

    it('given password empty, should not attempt to sign the user in', () => {
      component.email = 'example@example.com';
      component.password = '';

      component.signIn();

      expect(signinService.signin).not.toHaveBeenCalled();
    });

    it('given valid inputs and unsuccessful signin, should not navigate to dashboard', fakeAsync(() => {
      component.email = 'example@example.com';
      component.password = 'asdf1234';

      signinService.signin.and.returnValue(throwError({}));

      component.signIn();

      expect(router.navigate).not.toHaveBeenCalled();
      flush();
    }));
  });
});
