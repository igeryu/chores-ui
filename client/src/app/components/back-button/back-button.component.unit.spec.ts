import { BehaviorSubject, of } from 'rxjs';

import { BackButtonComponent } from './back-button.component';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

describe('BackButtonComponent (unit)', () => {
    let component: BackButtonComponent;
    let navigateBackService: jasmine.SpyObj<NavigateBackService>;

    beforeEach(() => {
        navigateBackService = jasmine.createSpyObj('NavigateBackService', ['events']);

        component = new BackButtonComponent(navigateBackService);
    });

    describe('ngOnInit:', () => {
        it('given a Navigate Back event, should set navigationCallback to event', () => {
            // Arrange
            const expectedFunction = (): void => { };
            navigateBackService.events.and.returnValue(new BehaviorSubject<() => void>(expectedFunction));
            component.navigationCallback = (): void => { const x = 2; };

            // Act
            component.ngOnInit();

            // Assert
            expect(component.navigationCallback).toBe(expectedFunction);
        });

        it('given a null Navigate Back event, should set navigationCallback to event', () => {
            // Arrange
            navigateBackService.events.and.returnValue(new BehaviorSubject<() => void>(null as any));
            component.navigationCallback = () => { };

            // Act
            component.ngOnInit();

            // Assert
            expect(component.navigationCallback).toBeNull();
        });
    });

    describe('navigateBack:', () => {
        it('should call navigationCallback', () => {
            // Arrange
            component.navigationCallback = jasmine.createSpy('mockNavigationCallback');

            // Act
            component.navigateBack();

            // Assert
            expect(component.navigationCallback).toHaveBeenCalled();
        });
    });
});
