import {
  Component,
  OnInit,
} from '@angular/core';

import { NavigateBackService } from 'src/app/services/navigate-back.service';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.css']
})
export class BackButtonComponent implements OnInit {
  public navigationCallback: (() => void) | null = null;

  constructor(private navigateBackService: NavigateBackService) { }

  ngOnInit(): void {
    this.navigateBackService.events().subscribe((callback) => {
      this.navigationCallback = callback;
    });
  }

  public navigateBack(): void {
    if (this.navigationCallback) {
      this.navigationCallback();
    }
  }

}
