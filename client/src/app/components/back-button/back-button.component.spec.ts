import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BackButtonComponent } from './back-button.component';

describe('BackButtonComponent', () => {
  let component: BackButtonComponent;
  let fixture: ComponentFixture<BackButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BackButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('given navigationCallback is not null, should have a back button', () => {
    // Arrange
    component.navigationCallback = () => {};

    // Act
    fixture.detectChanges();

    // Assert
    expect(fixture.nativeElement.textContent).toEqual('<');
  });

  it('given navigationCallback is null, should not have a back button', () => {
    // Arrange
    component.navigationCallback = null as any;

    // Act
    fixture.detectChanges();

    // Assert
    expect(fixture.nativeElement.textContent).toEqual('');
  });
});
