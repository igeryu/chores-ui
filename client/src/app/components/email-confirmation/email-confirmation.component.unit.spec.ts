import { Router } from '@angular/router';

import { EmailConfirmationComponent } from './email-confirmation.component';
import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { environment } from 'src/environments/environment';

describe('EmailConfirmationComponent (unit)', () => {
    let component: EmailConfirmationComponent;
    let navigateBackService: jasmine.SpyObj<NavigateBackService>;
    let router: jasmine.SpyObj<Router>;

    describe('ngOnInit', () => {

        beforeEach(() => {
            navigateBackService = jasmine.createSpyObj('NavigateBackService', ['push']);
            router = jasmine.createSpyObj('Router', ['navigate']);

            component = new EmailConfirmationComponent(
                router,
                navigateBackService,
            );
        });

        it('should push a "display Assigned Chores" call back to NavigateBackService', (done) => {
            // Arrange
            navigateBackService.push.and.callFake((pushedValue) => {
                // Validating pushed function
                // Act
                expect(router.navigate).toHaveBeenCalledTimes(0);
                expect(pushedValue).toBeTruthy('Expected callback function to be pushed');
                if (!pushedValue) {
                    fail('Expected callback function to be pushed');
                    return;
                }
                pushedValue();

                // Assert
                expect(router.navigate).toHaveBeenCalled();
                expect(router.navigate).toHaveBeenCalledTimes(1);
                expect(router.navigate).toHaveBeenCalledWith(environment.paths.signIn.navigation);
                done();
            });

            // Act
            component.ngOnInit();

            // Assert
            expect(navigateBackService.push).toHaveBeenCalled();
            expect(navigateBackService.push).toHaveBeenCalledTimes(1);
        });

    });
});
