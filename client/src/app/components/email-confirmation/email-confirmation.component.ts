import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NavigateBackService } from 'src/app/services/navigate-back.service';

import { environment } from 'src/environments/environment';

const SIGNIN_PATH = environment.paths.signIn.navigation;

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})
export class EmailConfirmationComponent implements OnInit {

  constructor(
    private router: Router,
    private navigateBackService: NavigateBackService,
  ) { }

  ngOnInit(): void {
    this.navigateBackService.push(() => this.router.navigate(SIGNIN_PATH));
  }

}
