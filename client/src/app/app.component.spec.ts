import { Component } from '@angular/core';
import {
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';

@Component({
  selector: 'app-back-button',
  template: ''
})
class MockAppBackButtonComponent { }

describe('AppComponent', () => {

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockAppBackButtonComponent,
      ],
      imports: [
        RouterTestingModule
      ],
    }).compileComponents();
  }));

  it('should create the app', waitForAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'Bounty Board App'`, waitForAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Bounty Board App');
  }));

  it('should have a back button', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const navBar = fixture.debugElement.query(By.css('nav.navbar'));
    const navBarChildren = navBar.queryAll(By.all());
    expect(navBarChildren.length).toEqual(2);

    const firstNavBarChild = navBarChildren[0];
    expect(firstNavBarChild).toBeTruthy();

    expect(firstNavBarChild.nativeElement.tagName.toLowerCase()).toEqual('app-back-button');
  });

});
