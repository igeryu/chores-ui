export interface Chore {
    id?: number;
    name: string;
}
