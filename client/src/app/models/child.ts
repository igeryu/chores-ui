export interface Child {
    id?: number;
    firstName: string;
    email: string;
}
