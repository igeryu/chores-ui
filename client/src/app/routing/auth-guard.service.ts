import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';

import { environment } from '../../environments/environment';
import { AuthService } from '../services/security/auth.service';
import { RouteAccessService } from '../services/security/route-access.service';

const SIGNIN_PATH = environment.paths.signIn.path;
const SIGNUP_PATH = environment.paths.signUp.path;

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService,
    private routeAccessService: RouteAccessService,
    private location: Location,
    private router: Router,
    ) { }

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    const userIsAuthenticated = this.authService.isAuthenticated();
    const joinedUrl = route.url.join();
    const accessingSignupOrSignInRoute = this.isSignInPath(joinedUrl) || this.isSignUpPath(joinedUrl);
    const notAccessingSignupOrSignInRoute = !accessingSignupOrSignInRoute;
    const userIsNotAuthenticated = !userIsAuthenticated;

    if (userIsAuthenticated && notAccessingSignupOrSignInRoute) {
      const currentUser = await this.authService.getCurrentUser();
      return this.routeAccessService.isUserAllowedToAccess(currentUser.role, joinedUrl);
    } else if (userIsNotAuthenticated && accessingSignupOrSignInRoute) {
      return true;
    } else if (userIsAuthenticated && accessingSignupOrSignInRoute) {
      this.location.back();
      return false;
    }
    this.router.navigate(environment.paths.signIn.navigation);
    return false;
  }

  private isSignUpPath(joinedUrl: string): boolean {
    return joinedUrl === SIGNUP_PATH;
  }

  private isSignInPath(joinedUrl: string): boolean {
    return joinedUrl === SIGNIN_PATH;
  }
}
