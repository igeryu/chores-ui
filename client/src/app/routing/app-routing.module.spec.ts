import { Route, Routes } from '@angular/router';

import { routes } from './app-routing.module';

import { environment } from '../../environments/environment';

import { AuthGuardService } from './auth-guard.service';

import { AddChoreComponent } from '../components/child-chores/add-chore/add-chore.component';
import { AssignChoresComponent } from '../components/child-chores/assign-chores/assign-chores.component';
import { AssignedChoresComponent } from '../components/child-chores/assigned-chores/assigned-chores.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { EmailConfirmationComponent } from '../components/email-confirmation/email-confirmation.component';
import { SignInComponent } from '../components/sign-in/sign-in.component';
import { SignupComponent } from '../components/signup/signup.component';

describe('AppRoutingModule routes', () => {
  let routesToTest: Routes;

  beforeEach(() => {
    routesToTest = [...routes];
  });

  it('should not have duplicate paths', () => {
    const checkedPaths: string[] = [];

    for (const routeToTest of routesToTest) {
      if (routeToTest.path) {
        expect(checkedPaths).not.toContain(routeToTest.path);
        checkedPaths.push(routeToTest.path);
      } else {
        expect(routeToTest.path).not.toBeNull();
        expect(routeToTest.path).not.toBeUndefined();
      }
    }
  });

  it('should redirect to signin when navigate to ""', () => {
    const indexRoute = findRouteByPath('');

    expect(indexRoute).toBeTruthy();
    expect(indexRoute?.redirectTo).toEqual(`/${environment.paths.signUp.route}`);
    expect(indexRoute?.pathMatch).toEqual('full');
  });

  it('should navigate to login correctly', () => {
    const signinRoute = findRouteByPath(environment.paths.signIn.route);

    expect(signinRoute).toBeTruthy();
    expect(signinRoute?.component).toEqual(SignInComponent);
    expect(signinRoute?.pathMatch).toEqual('full');
    expect(signinRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should navigate to home correctly', () => {
    const dashboardRoute = findRouteByPath(environment.startPage);

    expect(dashboardRoute).toBeTruthy();
    expect(dashboardRoute?.component).toEqual(DashboardComponent);
    expect(dashboardRoute?.pathMatch).toEqual('full');
    expect(dashboardRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should navigate to email confirmation', () => {
    const emailConfirmationRoute = findRouteByPath(environment.paths.emailConfirmation);

    expect(emailConfirmationRoute).toBeTruthy();
    expect(emailConfirmationRoute?.component).toEqual(EmailConfirmationComponent);
    expect(emailConfirmationRoute?.pathMatch).toEqual('full');
  });

  it('should navigate to signup correctly', () => {
    const signupRoute = findRouteByPath(environment.paths.signUp.route);

    expect(signupRoute).toBeTruthy();
    expect(signupRoute?.component).toEqual(SignupComponent);
    expect(signupRoute?.pathMatch).toEqual('full');
    expect(signupRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should navigate to Child\'s Chores screen correctly', () => {
    const childChoresRoute = findRouteByPath(environment.paths.child.chores.assigned.route);

    expect(childChoresRoute).toBeTruthy();
    expect(childChoresRoute?.component).toEqual(AssignedChoresComponent);
    expect(childChoresRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should navigate to Add Chore screen correctly', () => {
    const childChoresRoute = findRouteByPath(environment.paths.child.chores.add.route);

    expect(childChoresRoute).toBeTruthy();
    expect(childChoresRoute?.component).toEqual(AddChoreComponent);
    expect(childChoresRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should navigate to Assign Chore screen correctly', () => {
    const childChoresRoute = findRouteByPath(environment.paths.child.chores.assign.route);

    expect(childChoresRoute).toBeTruthy();
    expect(childChoresRoute?.component).toEqual(AssignChoresComponent);
    expect(childChoresRoute?.canActivate).toEqual([AuthGuardService]);
  });

  it('should redirect to home page when navigate to anything else', () => {
    const wildcardRoute = findRouteByPath('**');

    expect(wildcardRoute).toBeTruthy();
    expect(wildcardRoute?.redirectTo).toEqual(`/${environment.startPage}`);
  });

  function findRouteByPath(path: string): Route | null {
    for (const routeToTest of routesToTest) {
      if (routeToTest.path === path) {
        return routeToTest;
      }
    }
    return null;
  }
});
