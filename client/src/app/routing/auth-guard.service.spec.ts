import { Location } from '@angular/common';
import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

import { AuthGuardService } from './auth-guard.service';
import { AuthService } from '../services/security/auth.service';
import { RouteAccessService } from '../services/security/route-access.service';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

describe('AuthGuardService', () => {
  let authGuardService: AuthGuardService;
  let authService: jasmine.SpyObj<AuthService>;
  let location: jasmine.SpyObj<Location>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    const mockAuthService = jasmine.createSpyObj(['isAuthenticated', 'getCurrentUser']);
    const mockRouteAccessService = jasmine.createSpyObj(['isUserAllowedToAccess']);
    const mockLocation = jasmine.createSpyObj('Location', ['back']);
    const mockRouter = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      providers: [
        AuthGuardService,
        {provide: AuthService, useValue: mockAuthService},
        {provide: RouteAccessService, useValue: mockRouteAccessService},
        {provide: Location, useValue: mockLocation},
        {provide: Router, useValue: mockRouter},
      ],
    });
    authGuardService = TestBed.inject(AuthGuardService);
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
    location = TestBed.inject(Location) as jasmine.SpyObj<Location>;
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
  });

  it('should be created', () => {
    const service: AuthGuardService = TestBed.inject(AuthGuardService);
    expect(service).toBeTruthy();
  });

  describe('canActivate,', () => {

    describe('given user is already authenticated,', () => {
      let routeAccessService: jasmine.SpyObj<RouteAccessService>;

      beforeEach(() => {
        routeAccessService = TestBed.inject(RouteAccessService) as jasmine.SpyObj<RouteAccessService>;
        authService.isAuthenticated.and.returnValue(true);
      });

      describe('and accessing the signin page,', () => {
        let mockRouteSnapshot: jasmine.SpyObj<ActivatedRouteSnapshot>;
        let canActivate: boolean;

        beforeEach(async () => {
          const url = jasmine.createSpyObj('UrlSegment[]', ['join']);
          mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', [''], {
            url
          });
          url.join.and.returnValue(environment.paths.signIn.path);

          canActivate = await authGuardService.canActivate(mockRouteSnapshot);
        });

        it('should call join with "/"', () => {
          expect(mockRouteSnapshot.url.join).toHaveBeenCalled();
          expect(mockRouteSnapshot.url.join).toHaveBeenCalledWith();
        });

        it('should navigate back', () => {
          expect(location.back).toHaveBeenCalled();
        });

        it('should return false', () => {
          expect(canActivate).toBe(false);
        });

      });

      describe('and accessing a content page,', () => {

        it('should use the RouteAccessService', fakeAsync(() => {
          const expectedRole = 'ASDF_ROLE';
          authService.getCurrentUser.and.returnValue(new Promise<User>((resolve) => resolve({role: expectedRole} as User)));

          const url = jasmine.createSpyObj('UrlSegment[]', ['join']);
          const mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', [''], {
            url
          });
          
          const expectedRoute = 'asdf_route';
          url.join = () => expectedRoute;

          authGuardService.canActivate(mockRouteSnapshot);

          flush();
          expect(routeAccessService.isUserAllowedToAccess).toHaveBeenCalledWith(expectedRole, expectedRoute);
        }));

        [
          {expectedResponse: true},
          {expectedResponse: false},
        ].forEach(({expectedResponse}) => {
          it('should return the result from RouteAccessService', (done) => {
            authService.getCurrentUser.and.returnValue(new Promise<User>((resolve) => resolve({role: 'QWERTY_ROLE'} as User)));
            routeAccessService.isUserAllowedToAccess.and.returnValue(expectedResponse);

            const mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', ['']);
            mockRouteSnapshot.url = {
              join: jasmine.createSpy('join')
            };
            mockRouteSnapshot.url.join.and.returnValue('ui,bcde_route');

            authGuardService.canActivate(mockRouteSnapshot).then((result) => {
              expect(result).toBe(expectedResponse);
              done();
            });
          });
        });
      });

    });

    describe('given user is not authenticated,', () => {

      beforeEach(() => authService.isAuthenticated.and.returnValue(false));

      describe('and accessing the signin page', () => {

        it('should return true', (done) => {
          const mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', ['']);
          mockRouteSnapshot.url = {
            join: jasmine.createSpy('join')
          };
          mockRouteSnapshot.url.join.and.returnValue(environment.paths.signIn.path);

          authGuardService.canActivate(mockRouteSnapshot).then((canActivate) => {
            expect(canActivate).toBe(true);
            done();
          });
        });

      });

      describe('and accessing the signup page', () => {

        it('should return true', (done) => {
          const mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', ['']);
          mockRouteSnapshot.url = {
            join: jasmine.createSpy('join')
          };
          mockRouteSnapshot.url.join.and.returnValue(environment.paths.signUp.path);

          authGuardService.canActivate(mockRouteSnapshot).then((canActivate) => {
            expect(canActivate).toBe(true);
            done();
          });
        });

      });

      describe('and accessing any other page', () => {
        let mockRouteSnapshot: jasmine.SpyObj<ActivatedRouteSnapshot>;
        let canActivate: boolean;

        beforeEach(async () => {
          const url = jasmine.createSpyObj('UrlSegment[]', ['join']);
          mockRouteSnapshot = jasmine.createSpyObj('ActivatedRouteSnapshot', [''], {
            url
          });
          url.join.and.returnValue('/ui,asdf,not,re-direct');

          canActivate = await authGuardService.canActivate(mockRouteSnapshot);
        });

        it('should redirect to signin', () => {
          expect(router.navigate).toHaveBeenCalled();
          expect(router.navigate).toHaveBeenCalledWith(environment.paths.signIn.navigation);
        });

        it('should return false', () => {
          expect(canActivate).toBe(false);
        });

      });

    });
  });

});
