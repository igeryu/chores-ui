import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { environment } from 'src/environments/environment';

import { AddChoreComponent } from '../components/child-chores/add-chore/add-chore.component';
import { AssignedChoresComponent } from '../components/child-chores/assigned-chores/assigned-chores.component';
import { AssignChoresComponent } from '../components/child-chores/assign-chores/assign-chores.component';
import { AuthGuardService } from './auth-guard.service';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { EmailConfirmationComponent } from '../components/email-confirmation/email-confirmation.component';
import { SignInComponent } from '../components/sign-in/sign-in.component';
import { SignupComponent } from '../components/signup/signup.component';

const ASSIGN_CHORES_ROUTE = environment.paths.child.chores.assign.route;
const ADD_CHORES_ROUTE = environment.paths.child.chores.add.route;
const CHILD_CHORES_ROUTE = environment.paths.child.chores.assigned.route;
const DASHBOARD_ROUTE = environment.paths.dashboard.route;
const EMAIL_CONFIRMATION_ROUTE = environment.paths.emailConfirmation;
const SIGNIN_ROUTE = environment.paths.signIn.route;
const SIGNUP_ROUTE = environment.paths.signUp.route;

export const routes: Routes = [
  { path: CHILD_CHORES_ROUTE, component: AssignedChoresComponent, canActivate: [AuthGuardService] },
  { path: ADD_CHORES_ROUTE, component: AddChoreComponent, canActivate: [AuthGuardService] },
  { path: ASSIGN_CHORES_ROUTE, component: AssignChoresComponent, canActivate: [AuthGuardService] },

  { path: DASHBOARD_ROUTE, pathMatch: 'full', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: EMAIL_CONFIRMATION_ROUTE, pathMatch: 'full', component: EmailConfirmationComponent },
  { path: SIGNIN_ROUTE, pathMatch: 'full', component: SignInComponent, canActivate: [AuthGuardService] },
  { path: SIGNUP_ROUTE, pathMatch: 'full', component: SignupComponent, canActivate: [AuthGuardService] },
  { path: '', redirectTo: `/${SIGNUP_ROUTE}`, pathMatch: 'full' },
  { path: '**', redirectTo: `/${DASHBOARD_ROUTE}`, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
